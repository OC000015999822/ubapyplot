



..
    ######################### <<< DO NOT CHANGE ME ... >>> ##########################

..
	we do include the title page here | please make your changes into this page (@ ./_templates/title_page.md)


.. include:: ./_templates/title_page.md
    :parser: myst_parser.sphinx_


.. 
	We include the empty page to get an entrypoint as a node pointing to title page inside the side bar
	
.. include:: ./_templates/DO_NOT_CHANGE_AND_DELETE_ME.md
    :parser: myst_parser.sphinx_

..
    ########################### <<< ...UNTIL HERE >>> ###############################
    
    
.. 
    From here make your configurations and define the structure of your page... 
    ###########################################################################
    
    

##################
What it is about 1
##################

.. include:: ./page_struct/what_it_is_about/what_it_is_about.md
	:parser: myst_parser.sphinx_




##############
Installation 2
##############

.. include:: ./page_struct/installation/installation.md
	:parser: myst_parser.sphinx_



#############################
Corporate Design of the UBA 3
#############################

.. include:: ./page_struct/uba_cd/uba_cd.md
	:parser: myst_parser.sphinx_

    
##############################	
Basic Principle of UBAPyPlot 4
##############################

.. include:: ./page_struct/basic_principle_of_ubapylot/basic_principle_of_ubapyplot.md
	:parser: myst_parser.sphinx_






###########################################	
Figure Style Settings by Class Attributes 5
###########################################
.. include:: ./page_struct/figure_style_settings_by_class_attributes/figure_style_settings_by_class_attributes.md
	:parser: myst_parser.sphinx_


************************************
Configuration of the Figure Size 5.1
************************************
.. include:: ./page_struct/figure_style_settings_by_class_attributes/figure_size/figure_size.md
	:parser: myst_parser.sphinx_
	
	
****************************************
Child Axes Object Size Configuration 5.2
****************************************
.. include:: ./page_struct/figure_style_settings_by_class_attributes/size_of_child_axes_object/child_axis.md
	:parser: myst_parser.sphinx_	
	

**************************************************************
Configuration of the Boundary Box of the Child Axes Object 5.3
**************************************************************
.. include:: ./page_struct/figure_style_settings_by_class_attributes/boundary_box_of_child_axes_object/boundary_box_of_child_axes_object.md
	:parser: myst_parser.sphinx_	
	
	
*********************************************
Configuration of the Style Line Positions 5.4
*********************************************
.. include:: ./page_struct/figure_style_settings_by_class_attributes/style_line_position/style_line_position.md
	:parser: myst_parser.sphinx_
	
	
	
	
###############################################################	
Figure Style Settings by customized mpl style sheet templates 6	
###############################################################	
.. include:: ./page_struct/mpl_style_sheets/mpl_style_sheets.md
	:parser: myst_parser.sphinx_	
	

##########	
Subplots 7	
##########	
.. include:: ./page_struct/subplots/subplots.md
	:parser: myst_parser.sphinx_



	
###########################	
Boilerplate-Code/Examples 8	
###########################	
.. include:: ./page_struct/boilerplate_code/boilerplate_code.md
	:parser: myst_parser.sphinx_
		
	
*********************
Simple Line Chart 8.1
*********************
.. include:: ./page_struct/boilerplate_code/simple_line_plot/simple_line_plot.md
	:parser: myst_parser.sphinx_
	
	
**********************
Vertical Bar Chart 8.2
**********************
.. include:: ./page_struct/boilerplate_code/vertical_bar_chart/vertical_bar_chart.md
	:parser: myst_parser.sphinx_	
	

************************	
Horizontal Bar Chart 8.3	
************************	
.. include:: ./page_struct/boilerplate_code/horizontal_bar_chart/horizontal_bar_chart.md	
	:parser: myst_parser.sphinx_	
	
	
**********************	
Stacked Area Chart 8.4	
**********************	
.. include:: ./page_struct/boilerplate_code/stacked_area_plot/stacked_area_plot.md	
	:parser: myst_parser.sphinx_	
	

	
*************	
Pie Chart 8.5	
*************	
.. include:: ./page_struct/boilerplate_code/pie_plot/pie_plot.md	
	:parser: myst_parser.sphinx_		
	
	
***************	
Donut Chart 8.6	
***************	
.. include:: ./page_struct/boilerplate_code/donut_plot/donut_plot.md	
	:parser: myst_parser.sphinx_		
	

***************	
Polar Chart 8.7	
***************	
.. include:: ./page_struct/boilerplate_code/polar_plot/polar_plot.md	
	:parser: myst_parser.sphinx_	


****************	
Bubble Chart 8.8	
****************
.. include:: ./page_struct/boilerplate_code/bubble_chart/bubble_chart.md	
	:parser: myst_parser.sphinx_	



************	
Geo Plot 8.9	
************
.. include:: ./page_struct/boilerplate_code/geo_plot/geo_plot.md	
	:parser: myst_parser.sphinx_	


**********************	
Multi Y-Axis Plot 8.10	
**********************
.. include:: ./page_struct/boilerplate_code/multi_y_axes/multi_y_axes.md	
	:parser: myst_parser.sphinx_	



#########
Gallery 9
#########
.. include:: ./page_struct/gallery/gallery.md	
	:parser: myst_parser.sphinx_	
		
	
	
	
	
	
################# 	
Module Members 10
#################


UBA_Plot
********

.. autoclass:: UBAPyPlot.UBA_Plot.UBA_Plot
   :members:
   :special-members:
   :private-members:                                                                     
   :exclude-members: __dict__, __weakref__, __module__ 
   :member-order: bysource


style_sheet_manager
*******************

.. autoclass:: UBAPyPlot.style_sheet_manager.StyleSheetManager
   :members:                                                     
   :special-members:
   :private-members:                     
   :exclude-members: __dict__, __weakref__ , __module__ 
   :member-order: bysource






grid
****

.. autoclass:: UBAPyPlot.plugins.grid.Grid
   :members:                                                     
   :special-members:
   :private-members:                       
   :exclude-members: __dict__, __weakref__ , __module__
   :member-order: bysource




uba_style_sheets.styles
***********************


.. autoclass:: UBAPyPlot.uba_style_sheets.styles.styles
   :members:                                                     
   :special-members:
   :private-members:                       
   :member-order: bysource