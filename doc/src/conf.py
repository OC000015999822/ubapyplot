
import os
import sys
sys.path.insert(0, os.path.abspath('../..'))

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information


project = 'UBAPyPlot'
copyright = '2023, Umweltbundesamt'
author = 'Alexander Prinz'
release = ''

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration


# we add the MathJax-JavaScript Librarary to the page
mathjax_path = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML"


# uncomment the line below when using the autosectionlabel functionality
autosectionlabel_prefix_document = True



### we add here all necessary external packages 
extensions = [
    'myst_parser',           # to be able to use MarkDown instead of RestructuredText
    'sphinx.ext.autodoc',    # to be able to use the autodoc function for add automaticly python docstrings as code documentation into the webpage or LaTex file
    'sphinx.ext.viewcode',   
    'sphinx.ext.mathjax',    # to be able to use LaTex to write equations and stuff like that
    'sphinx_copybutton',     # to obtain a copy button within code cells
    'sphinx_math_dollar',
]




### define here what kind of module members you want to show when using the autodoc function
autodoc_default_options = {
    "members": True, 
    "undoc-members": True, 
    "private-members": True  # should be removed if you do not want to show private class members of your python classes (indicated by the two underscores infront of their names...)
    }



exclude_patterns = ['_build']


# add a associative list to map file suffix to markup language
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}


### we preset here some LaText stuff
latex_elements = {
    'preamble' : '\\graphicspath{{./_static/}}',
    'papersize':'a4paper',
    'pointsize':'12pt',
    'fontpkg': '\\usepackage{times}',
    'figure_align': 'htbp',
}



#html_theme = 'UBASphinx'
html_static_path = ['_static']

html_theme = 'pydata_sphinx_theme'

    