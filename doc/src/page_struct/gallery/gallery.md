This subpage contains some diagrams/figures created with UBAPyPlot.
In this case, the source code is not explicitly included, because some figures come 
from private data analyses or the code is related to other projects which are then referenced.


<br>
<br>
<img src="_static/fun_approx.png" style="background-color:white;" alt="Figure shows multi feature plot created with UBAPyPLot"/>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 25
		</b>
		<br>
		<br>
		A simple multi feature plot created with UBAPyPlot.<br>
		Randomly generated 2 dimensional datapoints with an exponential dependency of feature Y 
		(function value) to feature X (time). A function has been approximated by an invers modelling approach (parameter estimation) to these data points. It is shown as blue line.<br>
		By a Monte-Carlo-Simulation approach the uncertainty of the parameter estimation has been estimated and can be seen as an confidence interval (grey area).<br>
		Due to the assumption the point distribution relativly to the mean model is a gaussian distributed the prediction interval has been estimated as the distance between the 
		mean model and twice of the standard deviation of the distances to the mean (blue area).<br>
		<b>This figure is not part of a publication by the UBA and only an example.</b>
	</p>
</div>

<center>
	<hr style="height: 2px; width: 50%; color: grey; padding: 0px 50px 0px 50px;">
</center>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<br>
<br>
<center>
	<img src="_static/windrose.png" style="background-color:white;" alt="Figure shows a windrose created with UBAPyPLot" width="60%"/>
</center>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 26
		</b>
		<br>
		<br>
		A windrose chart created with UBAPyPlot.<br>
		The figure shows a windrose of the wind conditions at the site "Fahrländer See near Potsdam" within the time range 2021 by 2023.<br>
		The wind speed has been seperated into 7 classes of speed ranges. Those classes are labeld by a specific color that can be seen in the legend.<br>
		The figure shows the typical west wind dominated wind direction distribution.<br>
		<b>This figure is not part of a publication by the UBA and only an example.</b>
	</p>
</div>

<center>
	<hr style="height: 2px; width: 50%; color: grey; padding: 0px 50px 0px 50px;">
</center>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<br>
<br>
<img src="_static/dzu_art.png" style="background-color:white;" alt="Figure shows a stacked bar plot created with UBAPyPLot"/>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 27
		</b>
		<br>
		<br>
		A stacked bar chart created with UBAPyPlot.<br>
		The figure shows a stacked bar chart that is part of the UBA article "Nährstoffeinträge über Flüsse und Direkteinleiter in die Ostsee" (see here: <a href="https://www.umweltbundesamt.de/daten/wasser/ostsee/naehrstoffeintraege-ueber-fluesse-direkteinleiter#eintrage-der-zuflusse-aus-deutschland-">https://www.umweltbundesamt.de/daten/wasser/ostsee/naehrstoffeintraege-ueber-fluesse-direkteinleiter#eintrage-der-zuflusse-aus-deutschland-</a>) written by departement II 2.3 "Protection of the seas and polor regions".<br>
		Thanks to UBAPyPLot this figure has been created automaticly without any manual action.<br>
	</p>
</div>

<center>
	<hr style="height: 2px; width: 50%; color: grey; padding: 0px 50px 0px 50px;">
</center>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<br>
<br>

<center>
	<img src="_static/ir_spec_.png" style="background-color:white;" alt="Figure shows a subplot created with UBAPyPLot" width="70%"/>
</center>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 28
		</b>
		<br>
		<br>
		A subplot created with UBAPyPlot.<br>
		The figure shows a subplot of two line plots.<br>
		It is about near infrared absorbance of tablets within a NIR range of 600 by 1898 nm.<br>
		The data are open data (link can be seen in the chapture of the plot).<br>
		<b>This figure is not part of a publication by the UBA and only an example.</b>
	</p>
</div>

<center>
	<hr style="height: 2px; width: 50%; color: grey; padding: 0px 50px 0px 50px;">
</center>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>