*Matplotlib* offers beside to its explizit functionalities with *PyPlot* two different ways to style plot figures.
The key component of these two ways are the so-called RC parameters. 
RC parameters can be used to customize style settings of the plots to get an own individual style. These parameters are
managed by a *style sheet*.
<br>
<br>
Please have a look at *Matplotlib's* web page about cutomizing style sheets and RC parameters at the link below:
<br>
<br>
<a href="https://matplotlib.org/stable/tutorials/introductory/customizing.html">Customizing Matplotlib with style sheets and rcParams</a>
<br>
<br>
<br>
The user can either use a selected style sheet (or default one) that is a ASCII-text file that contains/manages the style parameters (```rcParams```) or overwrites the default rcParams explicit within the *python*-code
by *Matplotib's* functionality to modify the rcParam style sheet as assitioates list (dictionary) that is represented by the ```matplotlib.rcPrams``` object.
<br>
<br>
<br>
The following code snipped is from the original *Matplotlib* web page as an example to explain how to modify the RC Parameters within the ```matplotlib.rcParams``` object (as dictionary):
<br>
<br>
<br>
```python*-code
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from cycler import cycler
mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['lines.linestyle'] = '--'
data = np.random.randn(50)
plt.plot(data)
```
*The code snipped above represents the workflow how to overwrite explicit (within the runtime) the both style parameters ```lines.linewidth``` (the width of line object within a plot) and*
*```'lines.linestyle'``` (the kind of line that is plotted).*
<br>
<br>
More informations can be seen here:
<br>
<br>
<a href="https://matplotlib.org/stable/tutorials/introductory/customizing.html#runtime-rc-settings">Runtime rc settings</a>
<br>
<br>
<br>
<br>
<br>
If the user has not setted an individual *Matplotlib Style Sheet*, the standard style sheet is loaded and used automaticly.
<br>
<br>
This standard style sheet contains a bunch of default setting regarding all given plot elements like line styles, axes elements such as tickmark setting, labelsetting etc.
<br>
<br>
<br>
To achieve a more individual style in terms of *UBA's* Corporate Design, some *mpl style sheets* have been created for their corresponding use cases.
<br>
They usally should map a bunch of use cases without the need modify them.
<br>
Nethertheless, it is possible to modify them to create an own style. That can be necessary when creating "exotic" plots.
<br>
The user than has the possibility to use either the already descriped ```rcParams``` or to modify the Style Sheet file.
<br>
<br>
The *UBAPyPLot* class ```UBAPyPlot.style_sheet_manager``` (see here: <a href="#UBAPyPlot.style_sheet_manager.StyleSheetManager">UBAPyPlot.style_sheet_manager</a>) includes three
methods to handle the individual UBA Corporate Design associated Style Sheets.
<br>
<br>
There is a setter method to set the corresponding presetted Style Sheet (see here: <a href="#UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style">UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style</a>).
<br>
This method maps an API to *PyPlot's* ```plt.style.use()``` method what is to set the Style Sheet. The method is overloaded by either passing a path to a ```Mpl Style Sheet``` file or pass the name of the listed presetted
plot style that *Matplotlib* delivers together with its python package.
<br>
<br>
When using *UBAPyPlot*, the user now has to pass insteat of the ```UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style()``` method. This passed method has to obtain one of the names of the given UBA Style Sheet
objects.
<br>
<br>
Below, one can see how to set the UBA Style Sheet of vertical bar charts:
<br>
<br>
<br>
```python
### we set here the plot style to the UBA own costumized style sheet for creating vertical bar charts
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('v_bar_chart'))
```
*Code snipped that is used to set the Matplotlib Style Sheet to the individual UBA Style Sheet for plotting vertical bar charts. Insteat of passing the path to the Style Sheet or an Name, the user has to pass the ```style_sheet_manager.StyleSheetManager().set_style()```-method with the individual Style Sheet Name regarding the kind of plot.*
<br>
<br>
<br>
The basic principle is, that the ```style_sheet_manager.StyleSheetManager().set_style('v_bar_chart')```method automaticly pass the path of the recently project directory and the name of the Style Sheet to PyPlot's 
```plt.style.use()``` method. When the user run the script first time the Style Sheet will be written as ```.mplstyle``` file into the project directory where the python script is located. 
<br>
For each further call of this script this Style Sheet than will be loaded automaticly.
<br>
<br>
That means if the user wants to modify the plot style he can simply use the Style Sheet (```.mplstyle```) file and change the RC Parameter of it.
<br>
<br>
The presetted Style Sheets are defined as a *python dictionary* object in the module object ```UBAPyPlot.uba_style_sheets.styles.styles``` (see here: <a href="https://gitlab.opencode.de/OC000015999822/ubapyplot/-/blob/master/src/UBAPyPlot/uba_style_sheets/styles.py">UBAPyPlot.uba_style_sheets.styles.styles</a>)
<br>
<br>
<br>
The user can use the ```UBAPyPlot.StyleSheetManager.clean_up()``` method to automaticly remove the Mpl Style Sheet file after script has been run.



