

*UBAPyPlot* is a "nano-framework" based on <a href="https://matplotlib.org/">Matplotlib</a> for creating plots in the *UBA Corporate Design*. 
When working with this framework, many techniques and workflows remain the same or very similar 
to the regular use of <a href="https://matplotlib.org/stable/api/pyplot_summary.html#module-matplotlib.pyplot">Matplotlib's Pyplot</a>.  

Various types of plots can be created, including "exotic" types such as plots with polar coordinates or geodata visualizations. Basically all plots are possible, 
as they can also be created by *Matplotlib*.

A connection of *Pandas* and also *GeoPandas* is not a problem. 

The concept of this nano-framework is to make plots in *UBA's Corporate Design* as easy as possibly and automatable. 
It is aimed at internal employees as well as third parties who work on behalf of UBA and have to create illustrations/data visualizations/plot figures in the UBA corporate design.

Automation capability is precisely the reason why this framework was developed. Department II 2.3 "Protection of the seas and polar regions" uses the framework for the automated creation of result visualizations within various reporting workflows, 
some of which have been almost completely automated.

<br>
<br>

Some boiler plate code has been created and can be used to addapt your data case to it. 
<br>
Please have a look to the capture [Boilerplate-Code/Examples 7](#boilerplate-code-examples-7) and find 
your appropriate boilder plate code.

Although the framework already takes a lot of work off your shoulders through the boiler plate codes and therefore only very few parameterizations have to be made, it is still sometimes necessary to make certain readjustments. 
Therefore all functions/methods of all classes and modules were provided with appropriate possibilities, in order to make appropriate adjustments. 

<br>
<br>
<br>

<i>
	<b>INFO:</b>
	<br>
	<br>
	For detailed information on UBA's Corporate Design, please use the official document available at the following link:
</i>
<br>
<br>
<a href="https://www.umweltbundesamt.de/dokument/corporate-design-des-umweltbundesamtes">https://www.umweltbundesamt.de/dokument/corporate-design-des-umweltbundesamtes</a>



<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>