<hr style="height: 5px; width: 100%; color: grey;">

<br>
<br>


A Corporate Identity is a verry important part of any company and makes representative elements of those companies unique and recognizable. 
Thus, UBA, not only our company but also an important part of our environment, state and society, also needs a recocnizable and unique Corporate Design.

Our Corporate Design in terms of data visualization needs to be used to make those data visualizations consistently and associable with us.

For more information on UBA’s Corporate Design Guideline in terms of data visualization, please see the following subchapter. 
You will see and learn the basic elements of plot figures in UBA Corporate Design. There you will also find a link to the official UBA Corporate Design Guideline document.


# Link to the Document of the UBA Corporate Design Guide 3.1

You can find all necessary informations about the <cite>UBA Corporate Design</cite> in the corresponding Guidline by following Link:

<br>
<br>

<a href="https://www.umweltbundesamt.de/sites/default/files/medien/1410/dokumente/2019_uba_manual_screen.pdf" title="Link to the Document of the UBA Corporate Design" >Official Document of UBA's Corporate Design</a>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


# Figures in UBA Corporate Design 3.2

This chapter explains the UBA Corporate Design regarding figures.


An important feature, apart from the typical grey hatching within the Child Axes Object area, are the 4 horizontal style lines (see Figure 1 below). 
There are different text elements to declaire plot figure attributes such as figure number, title, subtitle, legend elements, footnote and data source informations. 
These text elements need to be written in their correct font style and font size. 
The already mentioned grey hatching is a particular part of the UBA “style DNA” of the Corporate Design and makes figures recocnizable and unique but also hard to plot them by alternative 
ways for creating plot figures. A further important part of the UBA Corporate Design are the specific colors. You always should try to use them.



> **Note**
> Please always contact our section PB2 if you are unsure about the UBA Corporate Design and do not change designs on your own. 
> Use the official UBA Corporate Design Guid (see capture <a href="#link-to-the-document-of-the-uba-corporate-design-guide-3-1">Link to the Document of the UBA Corporate Design Guide 3.1</a>)




The basic elements of a plot figure are its text elements such as figure number, title, subtitle, legend elements, 
footnote and data source informations as well the grey hatched area where the diagrams or other kinds of data visualization objects have to be placed.


In the following figure, you can see the scheme according to which figures are to be fundamentally designed in UBA’s corporate design. 
This scheme must be followed to create UBA Corporate Design compliant plot figures.

<br>
<br>
<br>

<img src="_static/ausschnitt_uba_cd_style_giude.png" alt="Figure shows the basic elements of a UBA-Corporate-Design-compliant figure."/>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 1</b><br><br>
		The Figure was taken from the original document of the UBA Corporate Design (Corporate Design des Umweltbundesamtes; p. 80).<br>
		It shows the basic elements and their atrubutations of a plot figure in UBA Corporate Design. The link to the document can be found in the upper part of this subchapter.
	</p>
</div>



<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

# Color Scheme 3.3

A further part of the UBA Corporate Design is the color scheme. There are 12 specific colors that should be used. Generally, it is also allowed to use other colors than these if one has to plot more objects with 
different colors or wants to use graded colored elements such as heat maps or something similar. 
However, these 12 colors in every case should be your primary choice, since these colors provide a consistend and recognizable style scheme.

<br>
<br>
<br>


<style>
	.myTable { 
		width: 80%;
		text-align: left;
		background-color: none;
		border-collapse: collapse; 
	}
	.myTable th { 
		background-image: linear-gradient(135deg, #454545 25%, #575757 25%, #575757 50%, #454545 50%, #454545 75%, #575757 75%, #575757 100%);
		background-size: 7px 7px;
		color: #e5e5e5; 
		height: 35px;
	}
		.myTable td, 
		.myTable th { 
		padding: 6px;
		border: 1px solid #d8d8d8; 

	}
</style>
<table class="myTable" style="margin: 0 0 0 10%; height: 300px;">
		<tbody><tr>
			<th>Color Name</th>
			<th>Expression</th>
			<th>Hex-Code</th>
			<th>RGB-Code**</th>
		</tr>
		<tr class="data_tab_row">
			<td><br <="" td="">
			</td><td><br <="" td="">		
			</td><td><br <="" td="">
			</td><td><br <="" td="">
		</td></tr>			
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Grün</td>
			<td style="background-color:#5EAD35;"></td>
			<td style="text-align:center; font-weight: bold;">#5ead35</td>
			<td style="text-align:center; font-weight: bold;">94, 173, 53</td>
		</tr>
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Dunkelgrün</td>
			<td style="background-color:#007626;"></td>
			<td style="text-align:center; font-weight: bold;">#007626</td>
			<td style="text-align:center; font-weight: bold;">0, 118, 38</td>
		</tr>
		<tr class="data_tab_row">
			<td><br <="" td="">
			</td><td><br <="" td="">		
			</td><td><br <="" td="">
			</td><td><br <="" td="">
		</td></tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Blau</td>
			<td style="background-color:#0b90d5;"></td>
			<td style="text-align:center; font-weight: bold;">#0b90d5</td>
			<td style="text-align:center; font-weight: bold;">0, 155, 213</td>
		</tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Dunkelblau</td>
			<td style="background-color:#005f85;"></td>
			<td style="text-align:center; font-weight: bold;">#005f85</td>
			<td style="text-align:center; font-weight: bold;">0, 95, 133</td>
		</tr>		
		<tr class="data_tab_row">
			<td><br <="" td="">
			</td><td><br <="" td="">		
			</td><td><br <="" td="">
			</td><td><br <="" td="">
		</td></tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Flieder</td>
			<td style="background-color:#9d579a;"></td>
			<td style="text-align:center; font-weight: bold;">#9d579a</td>
			<td style="text-align:center; font-weight: bold;">157, 87, 154</td>
		</tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Dunkelflieder</td>
			<td style="background-color:#622f63;"></td>
			<td style="text-align:center; font-weight: bold;">#622f63</td>
			<td style="text-align:center; font-weight: bold;">98, 47, 99</td>
		</tr>		
		<tr class="data_tab_row">
			<td><br <="" td="">
			</td><td><br <="" td="">		
			</td><td><br <="" td="">
			</td><td><br <="" td="">
		</td></tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Fuchsia</td>
			<td style="background-color:#ce1f5e;"></td>
			<td style="text-align:center; font-weight: bold;">#ce1f5e</td>
			<td style="text-align:center; font-weight: bold;">206, 31, 94</td>
		</tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Dunkelfuchsia</td>
			<td style="background-color:#83053c;"></td>
			<td style="text-align:center; font-weight: bold;">#83053c</td>
			<td style="text-align:center; font-weight: bold;">131, 5, 60</td>
		</tr>	
		<tr class="data_tab_row">
			<td><br <="" td="">
			</td><td><br <="" td="">		
			</td><td><br <="" td="">
			</td><td><br <="" td="">
		</td></tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Ocker</td>
			<td style="background-color:#fabb00;"></td>
			<td style="text-align:center; font-weight: bold;">#fabb00</td>
			<td style="text-align:center; font-weight: bold;">250, 187, 0</td>
		</tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Dunkelocker</td>
			<td style="background-color:#d78400;"></td>
			<td style="text-align:center; font-weight: bold;">#d78400</td>
			<td style="text-align:center; font-weight: bold;">215, 132, 0</td>
		</tr>		
		<tr class="data_tab_row">
			<td><br <="" td="">
			</td><td><br <="" td="">		
			</td><td><br <="" td="">
			</td><td><br <="" td="">
		</td></tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Hellgrau*</td>
			<td style="background-color:#f0f1f1;"></td>
			<td style="text-align:center; font-weight: bold;">#f0f1f1</td>
			<td style="text-align:center; font-weight: bold;">240, 241, 241</td>
		</tr>	
		<tr class="data_tab_row">
			<td style="font-style: italic;">UBA Dunkelgrau</td>
			<td style="background-color:#4b4b4d;"></td>
			<td style="text-align:center; font-weight: bold;">#4b4b4d</td>
			<td style="text-align:center; font-weight: bold;">75, 75, 77</td>
		</tr>			
	</tbody>
</table>

\* The color definition "Hellgrau" may need to be changed slightly if this hue is too similar to other colored elements. <br>
\*\* Python-users have to transform/recompute the RGB values into the NumPy-compliant form (values within 0 and 1 -> dividing RGB values by 256).


<div class="fig_capt">
	<p>
		<b>Tab. Nr.: 1</b><br><br>
		Table of all of the <cite>UBA</cite> colors and their hexadecimal- as well as RGB-codes.<br>
		This information on the official color scheme of the <cite>UBA Corporate Design</cite> was taken from the official document<br>
		(<cite><i>Corporate Design des Umweltbundesamtes</i></cite>; p. 20). 
	</p>
</div>



<hr style="height: 5px; width: 100%; color: grey;">

