This sub chapter explains how to set the size and aspect ratio of a plot figure in UBAPyPlot.


Setting the aspect ratio is basically done by setting the corresponding class attribute called `size`. 
There is a default setting that fits the aspect ratio to a normal and quite universal use case. But some plot kinds such as pie plots and donut plots or geo plots need an other aspect ratio to get a well defined visual style. 
To do those individual settings of the figure size and aspect ratio we have to pass the setting values through the class constructor while instatiating a class object of the UBAPyPlot.UBAPlot class.


As followed the workflow to set the figure size and aspect ratio is shown. If we talk about figure size and aspect ratio, we actually talk about the same thing, because the aspect ratio is defined by the two size dimensions of the plot figure. 
We do have only to pass a tuple of these two values to define the figure size as well as in parallel the aspect ratio.


The following Figure (Fig.-Nr.: 3) shows an empty plot figure based on the standard default size setting. As the respective `UBAPyPlot.UBA_Plot` class object has beeen created no setting attribute was passed by the class constructor.
The following code snipped shows the instantiating promt of those class object:


```python
### we instantiate an UBA_CD_PyPlot.UBA_Plot class object without any self-defined style settings
plot_obj = UBA_Plot.UBA_Plot()
```

<br>
<br>


<img src="_static/style_set_1.png" alt="Figure shows how an empty default plot would be shown. No figure style attributes have been changed."/>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 3</b><br><br>
		The Figure above shows an empty plot with a default size setting that is automaticly used when instantiating a <a href="https://oc000015999822.usercontent.opencode.de/ubapyplot/doc/src/_build/#UBAPyPlot.UBA_Plot.UBA_Plot">UBAPyPlot.UBA_Plot</a> class object without passing explicit the size attribute with a certain setting parametrization.
	</p>
</div>


<br>
<br>
<br>
<br>
<br>
<br>


Now we will change/overwrite the size attribute concretely by passing a new attribute definition by the class constructor. 
We will use a parametrization that results into a nearly quadratic plot figure with a figure width of 4 units and a figure heigth of 5 units.
The following code snipped shows the instantiating promt of those class object with overwritten size attribute:

<br>

```python
### we instantiate an UBAPyPlot.UBA_Plot class object with a figure width of 4 units and height of 5 units
plot_obj = UBA_Plot.UBA_Plot(size=(4, 5))
```

Note the passed tuple `size=(4, 5)`.


<br>
<br>
<br>

The result of this parametrization can be seen by the following figure:

<br>
<br>

<center>
	<img src="_static/4_5.png" width="45%" alt="The Figure above shows an empty plot with a self-defined size attribute with a figure width of 4 units and a figure height of 5 units. It can be seen that the aspect ratio has been changed by the passed size definition compared with the default case represented by figure #3."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 4</b><br><br>
		The Figure above shows an empty plot with a self-defined size attribute with a figure width of 4 units and a figure height of 5 units.<br>
		It can be seen that the aspect ratio has been changed by the passed size definition compared with the default case represented by figure #3.
		<br>
		<br>
		<b>Note: The size refers to the height and width of the whole figure, not the hatched area. The whole figure does include also the text elements.<br>
		In addition one can see that the Y-axis label goes over the hatched area what is not correct. We will discuss this below. 
		</b>
	</p>
</div>



We can see, that the Y-axis label of the Child Axes Object overlapes the plot area. This is because the aspect ratio of the Parent Axes Object is not well choosen in this case.
This will be discussed in more detail in the point below.

<br>
<br>
<br>

# Oversize Issues 5.1.1

The following figure shows an oversized figure with an inconvenient size definition. In this case a setting of `width=8` and `height=3` where used.
This leads into a overlapping issues of some figure elements such as the x-axis label to the 2'nd horizontal style line and the figure title with the 4'th horizontal style line. 
Furthermore, the spacing between the horizontal style lines where the plot title and plot title is located in between, is to less. It leads into the already discussed issue about the overlapping but it also induces a not well visual style.
You either can treat that by using an other setting of width to height ratio or use the further style attributes to tweak the style configuration.

Please see the other chapter points to learn which style attributes can be used for.

<br>
<br>

<center>
	<img src="_static/oversize.png" width="75%" alt="The Figure above shows an empty plot with a self-defined size attribute with a figure width of 8 units and a figure height of 3 units. This setting leads into overlapping issues of some text elements with the horizontal style lines and also into a general unbalanced ratio of all figure elements."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 5</b><br><br>
			The Figure above shows an empty plot with a self-defined size attribute with a figure width of 8 units and a figure height of 3 units.<br>
			This setting leads into overlapping issues of some text elements with the horizontal style lines and also into a general unbalanced ratio of all figure elements.
		</b>
	</p>
</div>


<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>