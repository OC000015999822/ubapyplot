Some of the (basic) style parameters are available as class attributes. Thus, they can be (re)setted by passing them to the class constructor when instantiating a class object. 
Those attributes are the positions of the 4 horizontal style lines, the figure size (width and height) and the size of the child axes object (width and height) 
as well as the boundary box settings of the child axes object to reshift it within the parent axis object if necessary.
<br>
<br>
The following sub chapters explain those attributes more detailed.
<br>
<br>



<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>