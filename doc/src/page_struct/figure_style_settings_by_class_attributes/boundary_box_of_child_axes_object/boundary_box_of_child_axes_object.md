The boundary box settings have to be made rather rarely, since most problems can be solved either by a more suitable figure size and/or by the size attributes of the Child Axes Object.


Nevertheless, use cases may arise that require direct access to the boundary box settings. In particular, moving a diagram within the Child Axes Object in only one direction is very well possible with this. 
In principle, the size attributes of the Child Axes Object always cause an even scaling in both directions of the corresponding dimension, 
since the boundary box is embedded in the center of the Child Axes Object. 

The following content of this chapter demonstrates, what exactly this setting means. 

<br>
<br>

<hr style="margin: 0 50px 0 50px; border-radius:5px;" size="8" color="#eff5eb">

<br>
<br>


The figure below (Fig.-Nr.: 10) shows a complete plot figure created with *UBAPyPlot*.
The grey frame within the grey hatched area around the donut diagram indicates the area of the *Child Axes* Object.
Exactly in top of this grey frame also lays a green frame, the so called Boundary Box of the *Child Axes* Object.


<br>
<br>

<center>
	<img src="_static/bbox_1.png" width="75%" alt="The Figure shows a complete Plot figure created with UBAPyPlot in form of a donut plot.
There is a grey frame around the diagram element that indicates the area of the corresponding Child Axes Object.
Exactly in top of this frame lays a second but green colored frame that represent the Boundary Box of the Child Axes Object. "/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 10</b><br><br>
The Figure shows a complete Plot figure created with UBAPyPlot in form of a donut plot.<br>
There is a grey frame around the diagram element that indicates the area of the corresponding Child Axes Object.<br>
Exactly in top of this frame lays a second but green colored frame that represent the Boundary Box of the Child Axes Object. 
		</b>
	</p>
</div>

<br>
<br>
<br>


By default, a boundary box always has the same dimensioning/size as the corresponding axis object. The coordinates of the Boundary Box are relative
coordinates and for each dimension in a float number range of [0, 1]. The origin of the Boundary Box has the coordinate tuple (0, 0) and is located at the
left bottom corner relative to the area of the corresponding Axes Object.<br>
The spanning coordinate tuple has its point at (1, 1) and is relative to its corresponding Axes Object area located at its right top corner. 
So, these both points means the diagonal of the Boundary Box and also of the origin Axes Object area.<br><br>
What a Bondary Box and its settings offers is as mentioned above, that one can shift objects within the corresponding Axes Object.
<br>
<br>
The following figure (Fig. No.: 11) shows a pie plot created with the UBAPyPlot framework.
It can be seen that the optical center of the pie plot is not exactly in the center of the Parent Axes Object area, 
although a boundary box is always exactly centered. The optical center and the area center are not basically identical.

<br>
<br>
<br>

<center>
	<img src="_static/bbox_2.png" width="75%" alt="The figure shows a complete plot figure created with UBAPyPlot in form of a pie plot to demonstate a optical unbalanced diagram.
The pie diagram is opticaly too much shifted to the right side corner of the grey hatched area. That leads into an inconvinient look."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 11</b><br><br>
The figure shows a complete plot figure created with UBAPyPlot in form of a pie plot to demonstate a optical unbalanced diagram.
The pie diagram is opticaly too much shifted to the right side corner of the grey hatched area. That leads into an inconvinient look. 
		</b>
	</p>
</div>

<br>
<br>
<br>

The following code snippet was used to instantiate a UBAPyPlot.UBA_Plot object. The Boundary Box basic settings were not changed.<br>
Only the size parameters of the parent axes object and the height of the child axes object were changed against their default settings. 

<br>
<br>

```python
### we instantiate an axes object that represents the child axes object corresponding to its parent axes object.
plot_obj = UBAPlot.UBA_Plot(size=(4, 6), height_child='100%')

### we fetch the child axes object
ax = plot_obj.get_child_ax()
```


<br>
<br>
<br>


In this section we will now solve the visual problem of the shifted pie chart and change the boundary box setting a bit for this. 
This setting will move the chart down a bit, but more importantly, it will move it a bit further to the left.<br><br>
We use following attribute definition to do the job:<br><br>
<cite>bbox_to_anchor = (-0.012, -0.015, 1, 1)</cite><br><br>
Figure #12 (see below) shows the result of this setting:	



<center>
	<img src="_static/bbox_3.png" width="75%" alt="The figure shows a complete plot figure created with UBAPyPlot in form of a pie plot with well configurated boundary box settings.
Contrary to the pie chart with the default boundary box settings (Fig #11), this pie chart shows a more visually balanced ratio thanks to a suitable boundary box setting."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 12</b><br><br>
The figure shows a complete plot figure created with UBAPyPlot in form of a pie plot with well configurated boundary box settings.
Contrary to the pie chart with the default boundary box settings (Fig #11), this pie chart shows a more visually balanced ratio thanks to a suitable boundary box setting. 
		</b>
	</p>
</div>


<br>
<br>
<br>

The following code snippet was used to instantiate a UBA_CD_PyPlot.UBA_Plot object.<br>
The Boundary Box basic settings were changed in this case by the bbox_to_anchor attribute in addition to the other but non relevant attribute settings. 

<br>
<br>
<br>

```python
### we instantiate an axes object that represents the child axes object corresponding to its parent axes object.
plot_obj = UBA_Plot.UBA_Plot(size=(4, 6), height_child='100%', bbox_to_anchor=(-0.012, -0.015, 1, 1))

### we fetch the child axes object
ax = plot_obj.get_child_ax()
```


<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>