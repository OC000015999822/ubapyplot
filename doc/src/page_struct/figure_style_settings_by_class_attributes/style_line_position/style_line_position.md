In addition to the gray diagonally hatched area within a UBA figure where the graphs or other visual elements of the data display are placed, 
the 4 horizontal lines are also a point of interest.<br><br>
The both first lines (seen from lower) spann an area where the legend elements of a diagram are placed.<br>
Within the second and third horizontal line, the grey hatched area is spanned and defines the area where the diagrams and other graphical elements are placed.<br>
At the top of this grey hatched area is the area where the title and sub-title of the plot figure is located. This area is defined be the third and forth horizontal style line.<br><br>
Normaly all plot elements such as the text elements, legend items and diagram are well located regarding these 4 horizontal lines.<br><br>
Nevertheless, under certain circumstances/parameterizations, some elements may collide with the lines.<br><br>
For this reason, the possibility was created to move the line individually within the parent axes object along the Y-direction in order to avoid or handle corresponding collisions.
It will probably be extremely rare that you will need to do any readjustment of these lines. However, if this is the case, this chapter will help you understand the corresponding class attributes.
<br><br><br>
To modify the 4 horizontal lines one can use their class attributs which define the relativ Y-height. The Range is [0, 1] while 0 means the bottom of the Parent Axes Object and 1 the top.<br><br>
There are following 4 attributs and their default values:

<br>
<br>


* `y_line_1` ← 0.08 (bottom line)
* `y_line_2` ← 0.16 
* `y_line_3` ← 0.8 
* `y_line_4` ← 0.9158 (top line)

<br>
<br>

The reason why the default value of the Y-height of the top line is not 1 and of the bottom one not 0 is, since there are still text elements above and below. 

<br>
<br>

The following figure (Fig.-Nr.: 13) shows a base case as a reference object to illustrate the effect of the corresponding style attributes. 
All further illustrations are derived from this in such a way that only the attribute values of the horizontal lines are changed.

In this case no settengs where made. The 4 horizontal line positions are defined by their default settings. 


<br>
<br>
<br>

<center>
	<img src="_static/y_line_1.png" width="75%" alt="The figure shows an empty plot figure created with UBAPyPlot. All elements are defined by their default values."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 13</b><br><br>
			The figure shows an empty plot figure created with UBA_CD_PyPlot.<br>
			All elements are defined by their default values. 
		</b>
	</p>
</div>

<br>
<br>
<br>

The following code snippet was used to instantiate a `UBAPyPlot.UBA_Plot` object.

<br>
<br>
<br>

```python
### we instantiate an axes object that represents the child axes object corresponding to its parent axes object.
plot_obj = UBAPlot.UBA_Plot()

### we fetch the child axes object
ax = plot_obj.get_child_ax()
```

<br>
<br>
<br>

Now we will move the third horizontal line (counted from the bottom) some downwards and use the class attribute `y_line_3` for this and set it to a value of 0.6 .
In addition we shift the 4'th horizontal line (`y_line_3`) some upwards and set its attribute value to 0.95 .

The following code snipped shows the used setting and below of that snipped the result plot figure is shown below this code snipped: 

<br>
<br>
<br>

```python
### we instantiate an UBAPyPlot.UBA_Plot class object with a shifted 3'rd line to the y-position 0.6
### ...as well the 4'th one at 0.94
plot_obj = UBAPlot.UBA_Plot(y_line_3=.6, y_line_4=.94)
```


<br>
<br>
<br>

The resulting plot figure: 

<br>
<br>
<br>

<center>
	<img src="_static/y_line_2.png" width="75%" alt="
The figure shows an empty plot figure created with UBAPyPlot.
All elements are defined by their default values except the class attribute value of the y_line_3 attribute.
It has been setted to 0.6 and the default value is 0.8. It can be seen that this smaller values results a downward shifted third horizontal style line that collides now with the area of the Child Axes Object.
Furthermore, the 4'th horizontal line element also has been shifted but in this case some upwards be setting its attribut value from 0.9158 (default) to 0.94. Now, this line collides with the text elemet to mark the figure number."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 14</b><br><br>

The figure shows an empty plot figure created with UBAPyPlot.
All elements are defined by their default values except the class attribute value of the `y_line_3` attribute.
It has been setted to 0.6 and the default value is 0.8. It can be seen that this smaller values results a downward shifted third horizontal style line that collides now with the area of the *Child Axes* Object.
Furthermore, the 4'th horizontal line element also has been shifted but in this case some upwards be setting its attribut value from 0.9158 (default) to 0.94. Now, this line collides with the text elemet to mark the figure number. 
		</b>
	</p>
</div>

<br>
<br>
<br>

Figure 14 shows the shifted horizontal lines. Of course, this display does not look so good. But this possibility to modify those attribute setting could be useful in some, but probably rare, cases. 


<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>