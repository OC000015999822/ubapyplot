This chapter explains how to set the size parameters of the child axes object in *UBAPyPlot*.


Sometimes this can be necessary, if the visuall elements of the `Parent Axes` object are well ballancing setted but the diagramm or wat the user wants to plot, is not optimal within the area of the grey hatching.

The user than has the possibilty to tweak this by the `Child Axes` size attributes and stretch the `Child Axes` object in Y- and/or X-direction. 
In contrast to the size settings of the `parent axes` object, two attributes must be defined separately here. In addition, relative size specifications in percent apply here, 
since the size of the `child axes` object refers to the size of the `parent axes` object.


This is made clearer by the following illustrations.

<br>
<br>
<br>

<center>
	<img src="_static/child_axis_1.png" width="75%" alt="The Figure shows a complete Plot figure created with UBAPyPlot plot framework.
The scope is here the location of the Child Axes Object within its Parent Axes Object. This location is indicated here by the grey frame around this Child Axes Object within the grey hatched area of the Parent Axes object.
The green frame around the donut diagram indicates the plot area of this diagram plot object.
It can be seen that the chart is a bit too small compared to the size of the gray shaded area and should have a larger size.
A attribute setteg of height_child="50%" has been used to obtain that smaller diagram."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 6</b><br><br>
			The Figure shows a complete Plot figure created with UBAPyPlot plot framework.<br>
			The scope is here the location of the Child Axes Object within its Parent Axes Object. This location is indicated here by the grey frame around this Child Axes Object within the grey hatched area of the Parent Axes object.<br>
			The green frame around the donut diagram indicates the plot area of this diagram plot object.<br>
			It can be seen that the chart is a bit too small compared to the size of the gray shaded area and should have a larger size.<br>
			A attribute setting of height_child="50%" has been used to obtain that smaller diagram.
		</b>
	</p>
</div>


<br>
<br>

By following code the UBAPyPlot.UBA_Plot class object has been instantiated:

<br>

```python
### we instantiate an UBAPyPlot.UBA_Plot object and set the height_child attribute to "50%"
plot_obj = UBA_Plot.UBA_Plot(height_child='50%')
```

<br>
<br>
<br>
<br>

The Figure above (Fig.-Nr.: 6) shows a complete plot figure created with UBAPyPlot but has still a inconvinient ratio of its element sizes.
The diagram in the midle of the grey hatched area is some too small, yet.

Since, the diagram is mapped to the `Child Axes` Object, we can now use the both size defining attributes of the `Child Axes` Object.

We define the parameter values as character strings and in percentage values.
There is the `height_child` attribute to stretch the ```Child Axes``` Object along the Y-axis and the ```width_child``` attribute to stretch the ```Child Axes``` Object along the X-axis.

If only one of these two attributes has to been redefined to resize the `Child Axes` Object, the other one will be adjusted in a linear wise.
That means, if you want your plot uniformly resized regarding both axes, you only need to use one of the attributes and redefine it.
For the case that you want to resize the `Child Axes` Object regarding its both directions/dimensions independently, you have to define both attributes with, of course, different values.

The following figure (Fig.-Nr.: 7) shows a similar plot figure like Figure #5, but the ```Child Axes``` Object has been uniformly resized by using a larger value of the ```height_child``` attribute (since we want a uniformly resized diagram, we do not need to define the ```width_child``` attribute too):


<br>
<br>

<center>
	<img src="_static/child_axis_2.png" width="45%" alt="The Figure shows a complete Plot figure created with UBAPyPlot plot framework again.
In this case can be seen compared to figure #5 that the donut diagram within the grey hatched area is some larger and since, it has a more better look.
The attribute setting of the attribute height_child is "100%" in this case."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 7</b><br><br>
			The Figure shows a complete Plot figure created with UBAPyPlot plot framework again.
			In this case can be seen compared to figure #1 that the donut diagram within the grey hatched area is some larger and since, it has a more better look.
			The attribute setting of the attribute height_child is "100%" in this case.
		</b>
	</p>
</div>

<br>
<br>
<br>
<br>


We will now see what is happening when we use a setting of both size attributes of the Child Axes Object with different values.

But we do consider now an example that is more suitable to show the impact of the two size attributes.

The following plot figure (Fig.-Nr.: 8, below) is a bar plot and hence, it has two different and independently axes.


The setting of the two size attributes is as followed:

<br>

* ```height_child="50%"```
* ```width_child="90%"```

Means the Object instatiating would be done by following code:

```python
### we instantiate an UBA_CD_PyPlot.UBA_Plot object
### ... and set the height_child attribute to "50%" and the width_child attribute to "90%"
plot_obj = UBA_Plot.UBA_Plot(height_child='50%', width_child='90%')
```


<br>
<br>

<center>
	<img src="_static/child_axis_3.png" width="75%" alt="The Figure shows a complete Plot figure created with UBAPyPlot in form of a bar plot with different classes maped two a anual time range of 1990 to 2010.
The size attribute setting of the Child Axes Object is height_child = '50%' and width_child = '90%'."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 8</b><br><br>
			The Figure shows a complete Plot figure created with UBAPyPlot plot framework again.
			In this case can be seen compared to figure #1 that the donut diagram within the grey hatched area is some larger and since, it has a more better look.
			The attribute setting of the attribute height_child is "100%" in this case.
		</b>
	</p>
</div>

<br>
<br>
<br>
<br>



Let's see now what happen if we set `height_child` to "20%" and keep the "90%" of attribute `width_child`.

The figure below (Fig.-Nr.:9) shows the result: 

<br>
<br>

<center>
	<img src="_static/child_axis_4.png" width="75%" alt="The Figure shows a complete Plot figure created with UBAPyPlot in form of a bar plot with different classes maped two a anual time range of 1990 to 2010.
The size attribute setting of the Child Axes Object is height_child = "20%" and width_child = "90%".
The aspect ratio of the Child Axes Object is not convinient and do not fit to its Parent Axes Object aspect ratio. Hence, this setting is definitely not a well setting."/>
</center>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 9</b><br><br>
			The Figure shows a complete Plot figure created with UBAPyPlot in form of a bar plot with different classes maped two a anual time range of 1990 to 2010.<br>
			The size attribute setting of the Child Axes Object is height_child = "20%" and width_child = "90%".<br>
			The aspect ratio of the Child Axes Object is not convinient and do not fit to its Parent Axes Object aspect ratio. Hence, this setting is definitely not a well setting.
		</b>
	</p>
</div>

<br>
<br>

We can see in figure #9 that the resized *Child Axes* Object regarding its Y-axis by the ```height_child``` attribute leads into an totaly other aspect ratio of the ```Child Axes``` Object of figure #8. 


<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>