To install the package, use PIP.
<br>
Usually it is enough to install the package directly from the repository. The repository always contains the current Python wheel, which can be found in the <a href="https://gitlab.opencode.de/OC000015999822/ubapyplot/-/tree/master/src/dist">/src/dist/</a> directory.
<br>
<br>
To install directly from the repo, the following promt can be used:
<br>
<br>

```shell
py -m pip install git+https://gitlab.opencode.de/OC000015999822/ubapyplot/src/dist/UBAPyPlot-1.4-py3-none-any.whl
```
<i>
Please note that this case explicitly concerns version 1.4. If there are other versions, the promt should be adjusted accordingly.
In addition, please note that your company may maintain a proxy server. In this case, this must appear in the promt the argument --proxy="URL_TO_MY_PROXY", where the string "URL_TO_MY_PROXY" must be adjusted accordingly.
Please contact your administrator if there are any problems.
In this case the path to the python interpreter executable has been setted as environmental variable called ```py```. Please change it if your variable name is different
or use the path to the executable insteat if you did not setted a environmental variable.
</i>

<br>
<br>
<br>
<br>

## Alternative ways 2.1

### Manual installation by downloading the WHL file 2.1.1

<br>
<br>
Another option is to manually download the desired Python-Wheel of the <i>UBAPyPLot-Package</i> and then install it from the download directory via PIP.
<br>
<br>
To do this, navigate within the GitLab repo to the directory containing the WHL files. To be found at the following link:
<br>
<br>
<a href="https://gitlab.opencode.de/OC000015999822/ubapyplot/-/tree/master/src/dist">/src/dist/</a>
<br>
<br>
Pick the appropriate wheel and click on it. A window will open with a download link.
<br>
<br>
After the download is complete, you can use the following prompt to install the package with PIP:
<br>
<br>

```shell
py -m pip install <the_path_to_your_download_directory>/<the_name_of_the_wheel_file.whl>
```

<br>
<br>
<br>
<br>

### Installation from the source 2.1.2

<br>
<br>
The second alternative option is a very advanced option and is aimed at when your own changes have been made in the code or the WHL files are not compatible with 
your computer architecture/operating system (WHL files were only brewed for Windows).

<br>
<br>

In this case, download the entire source to your computer and brew your own WHL file.
To do this, use git to clone the src directory or download the directory manually at following link:

<br>
<br>
<a href="https://gitlab.opencode.de/OC000015999822/ubapyplot/-/tree/master/src">/src</a>
<br>
<br>

Appropriate knowledge for creating WHL files is assumed here, which is why no detailed explanations are given here.
By the way, you can create a WHL file automatically with the file "mk_wheel.bat" via double-click without having to open bdistwheel 
via the setup script in the Python interpreter. Provided you are working on a Windows system. 




<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>