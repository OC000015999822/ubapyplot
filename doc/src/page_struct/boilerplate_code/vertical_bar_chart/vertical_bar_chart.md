<i>Keywords:<br>#Bar Chart; #Vertical Bar Chart; #Balkendiagramm; #vertikales Balkendiagramm</i><br><br>

This example is about a vertical Bar Chart with UBAPyPlot to plot those in the UBA Corporate Design. 
<br>
<br>
An example can be seen below:
<br>
<br>
<img src="_static/vertical_bar_chart.png" style="background-color:white;" alt="Figure shows a vertical bar chart created with UBAPyPLot"/>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 16
		</b>
		<br>
		<br>
		A vertical bar chart created by UBAPyPlot.<br>
	</p>
</div>
<br>
<br>
<br>
<br>




<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>



We load the necessary packages that we need.
<br>

```python

'''import external packages'''
import pandas as pd
import numpy as np
import random as rand
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

'''import the UBAPyPlot-Package'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```

<br>
<br>

We set the Matplotlib Plot Style to an own costumized style sheet. Since we want to plot a vertical bar chart we set the ```style_kind``` parameter to ```v_bar_chart```.
<br>

```python
### we set here the plot style to the UBA own costumized style sheet for creating vertical bar charts
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('v_bar_chart'))
```

<br>
<br>

We generate some dummy data as PandasDataFrame.
<br>

```python
### 4 data vectors | each element represent a temporal corrolated value (yearly)
x     = [1.1, 17.5, 40, 48, 52]
y     = [5.2, 19, 42, 56, 60]
z     = [2, 8, 70, 10.5, 25]
z_alt = [4, 16, 60, 20, 70]


### we create an index vector that we will use as x-axis ticks
index = [
    '1990', 
    '1995', 
    '2000',
    '2005', 
    '2010', 
    ]

### we arrange the data as *Pandas DataFrame* to than profit from its .bar() method
df = pd.DataFrame(
        {
            'X'                  : x,
            'Y'                  : y,
            'Z'                  : z,
            r'$Z_{alternative}$' : z_alt
        }, index=index
    )
```

<br>
<br>

We instantiate an UBA_plot class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual.
<br>

```python

### do instatiate the UBA_CD_PyPlot object
plot_obj = UBA_Plot.UBA_Plot(height_child='51%', width_child='88%')

### grab the child axes object
ax       = plot_obj.get_child_ax()
```

<br>
<br>

We do the plots on the axes object.
<br>

```python

### we plot the Data
df.plot.bar(
    ax=ax, 
    color=['#5EAD35', '#007626', '#0b90d5', '#005f85', '#08536f'], 
    edgecolor='#345663',
    linewidth=.5,
    legend=False, 
    zorder=1, 
    width=.7,
    rot=0
)
```

<br>
<br>

We call the ```set_stripes()```-method to get the fancy UBA stripes. Afterwards we use the ```mk_legend()```-method to get the legend.
<br>

```python

### plot the UBA stripes
plot_obj.set_stripes()


### plot the legend
plot_obj.mk_legend()
```

<br>
<br>

We set all text definitions like:

* Figure Number
* Title
* Subtitle
* Footnote
* Source informations

<br>

```python

### set here the figure number text
plot_obj.set_fig_number_text(
    text=u'Exemplarische Abbildung \u2192 nicht Bestandteil einer öffentlichen Publikation des UBA',
)

### set here the title of the figure
plot_obj.set_title_text(
    text='Beispiel eines vertikalen Balkendiagrams',
)

### set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='Datengrundlage ist fiktiv',
)

### set here source informations
plot_obj.set_source_info_text(
    text='Hier würden die Angaben zu den Quellen stehen',
)

### set here the footnote of the figure
plot_obj.set_footnote_text(
    text='\xa9 Umweltbundesamt, 2023',
)
```

<br>
<br>

We set a horizontal grid (only horizontal gridlines).

```python
ax.grid(axis='y', linestyle=':', color='#656565', lw=.6)
```

<br>
<br>

We set the axes labels and call the plot output by the ```plt.show()```-method. Afterwards we delate the style sheet by using the ```clean_up()```-method of the Style_Sheet_Manager class.
<br>

```python

### set the axes labels and grid
ax.set_xlabel('Datum')
ax.set_ylabel('Y-Axis Label')

### we add a grid (only horizontal and dashed line) to the axes object
ax.grid(axis='y', linestyle='--', color='#656565', lw=.5)

### make the plot
plt.show()


### we remove the temporary stylesheet
style_sheet_manager.StyleSheetManager().clean_up()
```