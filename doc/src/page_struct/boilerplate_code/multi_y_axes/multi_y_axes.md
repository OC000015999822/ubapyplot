<i>Keywords:<br>#multi axes; #2 axes; #different y axes</i><br><br>

This example is about a multi Y-axis plot with UBAPyPlot to plot those in the UBA Corporate Design. 
<br>
<br>
An example can be seen below:
<br>
<br>
<img src="_static/2_y_axes.png" style="background-color:white;" alt="Figure shows an simple line plot created with UBAPyPLot"/>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 24
		</b>
		<br>
		<br>
		A simple combined chart with a bar chart as well as a line chart created by UBAPyPlot.<br>
		Both of them have their own Y-axis as own scale.
	</p>
</div>
<br>
<br>
<br>
<br>




<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>



We load the necessary packages that we need.
<br>

```python

'''import external packages'''
import pandas as pd
import random as rand
from matplotlib import pyplot as plt

from datetime import date # we use an automatic date string replacement within the source text element


'''UBA_CD_PyPlot stuff'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```

<br>
<br>

We create some dummy-data to visualize it as example
<br>

```python
### dummy-Daten zum plotten

## Bar-Plot-Daten
df_bar_chart = pd.DataFrame(
    {
        't' : [i + 2000 for i in range(10)], # Zeitindex t
        'y' : [10*i + 100 + rand.gauss(0, 5) for i in range(10)] # random y
    }
)

print(df_bar_chart)



## Linien-Plot-Daten
df_line_chart = pd.DataFrame(
    {
        't' : [i + 2000 for i in range(10)], # Zeitindex t
        'y' : [abs(100*i + 100 + rand.gauss(0, 205)) for i in range(10)] # random y
    }
)

print(df_line_chart)
```

<br>
<br>

We set the Matplotlib Plot Style to an own costumized style sheet.
<br>

```python
### we set here the plot style to the UBA own costomized style sheet
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('v_bar_chart'))
```

<br>
<br>

We instantiate an UBA_plot class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual.
We do also set some specific style attributes and pass them to the class constructor.
<br>
<br>
Contrary to the usual procedure, this time we will not name the Axen object ```ax``` but ```ax_left```, because we will create a twin to this object, which we will then call ```ax_right```. 
The semantic shema should be clear with this. So both objects map to the respective Y-axis, which will get its own scale.

<br>

```python

# wir instanziieren ein Plot-Objekt und definieren einige style-Parameter
plot_obj = UBA_Plot.UBA_Plot(size=(9, 5), width_child='78%', height_child='40%', bbox_to_anchor=(-0.005, 0.03, 1, 1))


# wir lassen uns das korrespondierende Achsenobjekt zurückgeben und binden es an eine Variable
# wir nennen sie gleich ax_left, da auf diese die linke Y-Achse abbilden soll
ax_left = plot_obj.get_child_ax()
```

<br>
<br>

Now we have to instantiate a twin of ```ax_left``` that will represents than the right sided Y-axis 


We do the plots on the axes object.
<br>

```python

# wir instanziieren das Achsenobjekt für die Rechte Y-Achse
ax_right = ax_left.twinx()
```

<br>
<br>

We do the plot stuff and use the build-in-methods of PandasDataFrames
<br>

```python
# Bar-Plot bounded to the left Y-axis
df_bar_chart.plot.bar(
    x='t',            # here we have to define the index but only since the left axis object is the master of the twins
    y='y',
    ax=ax_left,
    width=.6,
    color='#0b90d5',
    legend=False,
    label=r'$Y_{1}(t)$'
)


# Linien-Plot gebunden an die rechte Y-Achse  
'''
Hier ist zu beachten, dass bei dem Plot bzgl. der rechten Achse der Index nicht definiert werden darf (wird automatisch vom Eltern-Objekt geerbt).
Andernfalls wird das Objekt nicht geplottet...warum auch immer...weder Bug noch Feature aber definitiv nicht cool.
'''

df_line_chart.plot(
    # x='x'           #we must not define the index, since, this object is mapped to the slave of the twins
    y='y',            # nur der Vektor mit den Y-Daten darf definiert werden
    ax=ax_right,
    color='#007626',
    lw=2.5,
    zorder=999,
    legend=False,
    label=r'$Y_{2}(t)$'
)
```

<br>
<br>

We do some styling stuff
<br>

```python 
## Styling

# y-axes labels
ax_left.set_ylabel(r'$Y_{1}$', rotation=0, labelpad=10)
ax_right.set_ylabel(r'$Y_{2}$', rotation=0, labelpad=10)


# x-axis label
ax_left.set_xlabel('Datum', fontsize=10)


# rotieren der X-Tick-Labels
plt.xticks(rotation = 45)


# plot the UBA stripes
plot_obj.set_stripes()


# plot the legend
plot_obj.mk_legend()


# we hide the both y-axes spines
ax_right.spines.right.set(visible=False)
ax_left.spines.left.set(visible=False)

# we hide the spine of the top
ax_left.spines['top'].set_visible(False)


ax_left.set_ylim([0, df_bar_chart['y'].max()])
ax_right.set_ylim([0, df_line_chart['y'].max()])


ax_left.tick_params(axis='x', which='major', labelsize=8)
ax_left.tick_params(axis='y', which='major', labelsize=8)

ax_right.tick_params(axis='x', which='major', labelsize=8)
ax_right.tick_params(axis='y', which='major', labelsize=8)
```

<br>
<br>

We set the text elements
<br>

``` python
# set here the title of the figure
plot_obj.set_title_text(
    text='Ein Beispiel für einen 2-Axen-Plot',
)

# we set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='(Hier steht der Sub-Titel)', 
    fontsize=9
    )


# set here source informations
plot_obj.set_source_info_text(
    text=f'Quelle: Umweltbundesamt {date.today().year}, Daten von wem auch immer...',                                                                          
    #xy=(.495, 0.02),   is deprecated since version 1.3
)


# set here the footnote of the figure
plot_obj.set_footnote_text(
    text=f'\xa9 Umweltbundesamt, {date.today().year}',
)
```

<br>
<br>

We call the ```plt.show()```-function to obtain our plot within a new window
<br>

```python 

plt.show()
```

