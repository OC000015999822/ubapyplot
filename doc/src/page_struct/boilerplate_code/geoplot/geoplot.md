<i>Keywords:<br>#Geodata; #Geoplot; #SHP; #geojson; #geopandas; /i><br><br>

This example is about a very simple geoplot with UBAPyPlot to plot those in the UBA Corporate Design. 
<br>
<br>
An example can be seen below:
<br>
<br>
<img src="_static/simple_line_chart.png" style="background-color:white;" alt="Figure shows an simple line plot created with UBAPyPLot"/>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 17
		</b>
		<br>
		<br>
		A simple line plot created by UBAPyPlot.<br>
	</p>
</div>
<br>
<br>
<br>
<br>




<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>



We load the necessary packages that we need.
<br>

```python

'''import external packages'''
import matplotlib.pyplot as plt
import pandas as pd
import random as rand

'''import the UBA_CD_PyPlot-Package stuff'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```

<br>
<br>

We set the Matplotlib Plot Style to an own costumized style sheet. Since we want to plot a line chart we set the ```style_kind``` parameter to ```standard_chart```.
<br>

```python
### we set here the plot style to the UBA own costomized style sheet
### corresponding to the line chart plot
plt.style.use(style_sheet_manager.StyleSheetManager().set_style(style_kind='standard_chart'))
```

<br>
<br>

We generate some random dummy data as PandasDataFrame.
<br>

```python
### create a list of values that will represent the x-axis data
X = range(10)

### create a list of some randomized dummy data corresponding to the y-axis
Y_1 = [x + rand.gauss(0, 0.5) for x in X]

### create a list of some randomized dummy data corresponding to the y-axis
Y_2 = [x + rand.gauss(0, 0.5) for x in X]

### create a list of some randomized dummy data corresponding to the y-axis
Y_3 = [x + rand.gauss(0, 2) for x in X]


### we do arrange those data as a Pandas DataFrame
df = pd.DataFrame(
	{
		'X'   : X,
		'Y_1' : Y_1,
		'Y_2' : Y_2,
		'Y_3' : Y_3
	}
)
```

<br>
<br>

We instantiate an UBA_plot class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual.
<br>

```python

### instantiate an UBA_plot-class object
plot_obj = UBA_Plot.UBA_Plot(height_child='45%', width_child='80%')


### get the child axes object
ax = plot_obj.get_child_ax()
```

<br>
<br>

We do the plots on the axes object.
<br>

```python

### plot X and Y_1, Y_2, Y_3 
ax.plot(
	df['X'].values,
	df['Y_1'].values,
        'o-',
	color='#5EAD35',
	label=r'$y_{1}(x)$',
	lw=.8
)

### plot X and Y_2 as line plot
ax.plot(
	df['X'].values,
	df['Y_2'].values,
        'o-',
	color='#0b90d5',
	label=r'$y_{2}(x)$',
	lw=.8
)


### plot X and Y_2 as line plot
ax.plot(
	df['X'].values,
	df['Y_3'].values,
        'o-',
	color=(157/255, 87/255, 154/255),
	label=r'$y_{3}(x)$',
	lw=.8
)
```

<br>
<br>

We call the ```set_stripes()```-method to get the fancy UBA stripes. Afterwards we use the ```mk_legend()```-method to get the legend.
<br>

```python

### set the UBA stripes hatch
plot_obj.set_stripes()

### make the legend 
plot_obj.mk_legend()
```

<br>
<br>

We set all text definitions like:

* Figure Number
* Title
* Subtitle
* Footnote
* Source informations

<br>

```python

### set here the figure number text
plot_obj.set_fig_number_text(
    text='Figure Number k',
)

### set here the title of the figure
plot_obj.set_title_text(
    text='Title of Figure',
)

### set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='Subtitle of Figure',
)

### set here source informations
plot_obj.set_source_info_text(
    text='Some informations about the data source',
)

### set here the footnote of the figure
plot_obj.set_footnote_text(
    text='* A Footnote',
)
```

<br>
<br>

We set the axes labels and call the plot output by the ```plt.show()```-method. Afterwards we delate the style sheet by using the ```clean_up()```-method of the Style_Sheet_Manager class.
<br>

```python

### set the axes labels and grid
ax.set_xlabel('X-Axis Label')
ax.set_ylabel('Y-Axis Label')

### we add a grid (only horizontal and dashed line) to the axes object
ax.grid(axis='y', linestyle='--', color='#656565', lw=.5)

### make the plot
plt.show()


### we remove the temporary stylesheet
style_sheet_manager.StyleSheetManager().clean_up()
```

