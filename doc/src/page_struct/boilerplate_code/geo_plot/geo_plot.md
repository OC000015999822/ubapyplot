<i>Keywords:<br>#geo plot; #geo chart; #shp; #geojson; #geopandas</i><br><br>

This example is about a geo plot created with UBAPyPlot and GeoPandas to plot those in the UBA Corporate Design. 
<br>
<br>
An example can be seen below:
<br>
<br>
<center>
	<img src="_static/geo_plot.png" style="background-color:white;" alt="Figure shows a geo plot created with UBAPyPLot and GeoPandas" width="50%"/>
</center>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 23
		</b>
		<br>
		<br>
		A simple geo plot created by UBAPyPlot and GeoPandas.<br>
	</p>
</div>
<br>
<br>
<br>
<br>

<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>


We load the necessary packages that we need.
<br>

```python

'''import external packages'''
import pandas as pd
import geopandas as gpd
from requests import Request
from owslib.wfs import WebFeatureService
import fiona
from fiona.drvsupport import supported_drivers
supported_drivers['WFS'] = 'r'
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.lines import Line2D



'''import the UBA_CD_PyPlot-Package stuff'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```


We fetch the geo data of the federal area and the municipal administration areas from a WFS server.<br>
We use the <i>Geodatenzentrum</i> as source and load the dataset from here:
<br>
<br>
<a href="https://sgx.geodatenzentrum.de/wfs_vg5000_1231">https://sgx.geodatenzentrum.de/wfs_vg5000_1231</a>
<br>
<br>

```python
###### fetch data from WFS Server

url = "https://sgx.geodatenzentrum.de/wfs_vg5000_1231"


wfs11 = WebFeatureService(url=url, version='1.1.0')

#print(wfs11.identification.title)
#print(list(wfs11.contents))



# Specify the parameters for fetching the data
layer = list(wfs11.contents)[-1]
params = dict(service='WFS', version="1.0.0", request='GetFeature',
      typeName=layer, outputFormat='json')


# Parse the URL with parameters and get response from wfs server
response = Request('GET', url, params=params).prepare().url


# Read data from URL
data_gdf = gpd.read_file(response)


#print(data_gdf.crs)

# we set the projection to WGS84
data_gdf = data_gdf.to_crs(4326)



# we define an other geo data DataFrame that represent the location of the UBA as well
# the BMUV 

lat_uba = 51.842237
lon_uba = 12.239279

lat_bmuv = 52.50732
lon_bmuv = 13.37702


df_features = pd.DataFrame(
    {
        'Location' : ['Hauptstandort\ndes UBA', 'Hauptstandort\ndes BMUV'],
        'colors'   : ['#007626', '#0b90d5'], # we use of course only the allowed colors ;)
        'lat'      : [lat_uba, lat_bmuv],
        'lon'      : [lon_uba, lon_bmuv]    
    }
) 

gdf_features = gpd.GeoDataFrame(
    df_features, 
    geometry=gpd.points_from_xy(df_features.lon, df_features.lat)
    )




### we set the projection to WGS84 UTM 32
gdf_features = gdf_features.set_crs(4326)
```

<br>
<br>

We set the Matplotlib Plot Style to an own costumized style sheet.
<br>

```python
### we set here the plot style to the UBA own costomized style sheet
### corresponding to the line chart plot
plt.style.use(style_sheet_manager.StyleSheetManager().set_style(style_kind='geo_data_chart'))
```

<br>
<br>

We instantiate an UBA_plot class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual.
We change some class attributes by passing their new definitions to the class constructor.
<br>
<br>

```python

### instantiate an UBA_plot-class object
plot_obj = UBA_Plot.UBA_Plot(size=(4.2, 6), height_child='62%', width_child='83%')

### get the child axes object
ax = plot_obj.get_child_ax()
```

<br>
<br>

We do the plot by using the ```plot()-method``` implemented in the GeoPandas DataFrames onto the both geodata DataFrames.

```python

### federal area
data_gdf.plot(ax=ax, color='#5EAD35', label='Bundesgebiet', zorder=1)

### geo locations of the UBA and BMUV
gdf_features.plot(ax=ax, column='Location', color=gdf_features.colors, zorder=2)

```

<br>
<br>

We set the UBA stripes by calling the ````set_stripes()```-method onto the axes object

```python
### set program to plot the UBA stripes
plot_obj.set_stripes()
```

<br>
<br>

We set the text element definitions

```python
### set here the figure number text
plot_obj.set_fig_number_text(
    text='Figure Number',
)

### set here the title of the figure
plot_obj.set_title_text(
    text='Title of Figure',
)

### set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='Sub-Title of Figure',
)

### set here source informations
plot_obj.set_source_info_text(
    text='Source: Information about the data source',
)

### set here the footnote of the figure
plot_obj.set_footnote_text(
    text='* Footnote',
)
```

<br>
<br>

We call the ```mk_legend()```-method and pass some optional parameters to make the legend as workaround to the nomal way

```python
### make legend with costum handlyes since GeoPandas plot API does not provide a solution
plot_obj.mk_legend( 
    fontsize=6,
    custom_handles=(
        Patch(
            facecolor='#5EAD35', 
            #edgecolor='w',
            label='Bundesgebiet'
        ),
        Line2D(
            [0],
            [0],
            marker='o',
            linestyle='none',
            color=gdf_features.iloc[0].colors, 
            #edgecolor='w',
            label=gdf_features.iloc[0].Location
        ),
        Line2D(
            [0],
            [0],
            marker='o',
            linestyle='none',
            color=gdf_features.iloc[1].colors, 
            #edgecolor='w',
            label=gdf_features.iloc[1].Location
        ),                     
    )
)
```

<br>
<br>

We set off the x- and y-ticks 

```python
plt.xticks([])
plt.yticks([])
```


We clean up the working directory of the created mplstylesheet and call the ```show()```-method to obtain the plot as new window

```python
### we remove the temporary stylesheet
style_sheet_manager.StyleSheetManager().clean_up()

plt.show()
```


