In this chapter, examples of various illustrations are shown. However, not only the results are presented, but also how these examples were implemented with code.
<br>
<br>
You may find examples that are similar to your use case and you can easily adapt the code to your case.
<br>
<br>


<div style="margin: 0 0 0 25px; font-size: 30px; background-color: rgba(255, 227, 193, 0.4); padding: 10px 10px 10px 10px;">
	<p>
		<i><b>
				<u>NOTE:</u><br><br>
				This documentation assumes familiarity with Matplotlib's PyPlot and therefore does not go into detail about the individual settings regarding parameters 
				of PyPlots's individual plot methods and utilities.
		</i></b>
	</p>
</div>


<br>
<br>
<br>
<br>


