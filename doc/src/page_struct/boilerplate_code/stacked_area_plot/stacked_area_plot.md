<i>Keywords:<br>#area plot; #stacked area plot;</i><br><br>

This example is about a stacked area plot created with UBAPyPlot to plot those in the UBA Corporate Design. 
<br>
<br>
An example can be seen below:
<br>
<br>
<img src="_static/stacked_area.png" style="background-color:white;" alt="Figure shows a stacked area plot created with UBAPyPLot"/>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 18
		</b>
		<br>
		<br>
		A stacked area plot created by UBAPyPlot.<br>
	</p>
</div>
<br>
<br>
<br>
<br>

<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>


We load the necessary packages that we need.
<br>

```python

'''import external packages'''
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
from datetime import date


'''import the UBAPyPlot-Package'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```

<br>
<br>

We set the Matplotlib Plot Style to an own costumized style sheet.
<br>

```python
### we set here the plot style to the UBA own costumized style sheet
### corresponding to the line chart plot
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('v_bar_chart'))
```

<br>
<br>

We create some dummy data that we than visualize

```python
# Create data
x=range(0, 8)
y1=[1, 4, 6, 8, 9, 10, 9, 8]
y2=[2, 2, 7, 10, 12, 14, 13, 12]
y3=[2, 8, 5, 10, 6, 5, 3, 1]
```

<br>
<br>

We instantiate an UBA_plot class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual.
<br>

```python

### instantiate an UBA_plot-class object
plot_obj = UBA_Plot.UBA_Plot(height_child='45%', width_child='80%')


### get the child axes object
ax = plot_obj.get_child_ax()
```
<br>
<br>

We do the plot action by using the ```stackplot()```-method to the axes object and pass the data that we have created before
Furthermore we pass a corresponding label- and color list to have a color sheme and get a legend mapping. 

```python
ax.stackplot(
	x, 
	y1, 
	y2, 
	y3, 
	labels=[r'$\alpha$', r'$\beta$', r'$\gamma$'], 
	colors=['#007626', '#005f85', '#622f63'], 
	alpha=.8
	)
```

<br>
<br>

We set the limitation of the both axes

```python
ax.set_xlim([0, 7])
ax.set_ylim([0, 30])
```

<br>
<br>

We set the text definitions

```python
### set here the figure number text
plot_obj.set_fig_number_text(
    text=u'Exemplarische Abbildung \u2192 nicht Bestandteil einer öffentlichen Publikation des UBA',
)

### set here the title of the figure
plot_obj.set_title_text(
    text='Gestapeltes Liniendiagramm mit Flächenfüllung',
)

### set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='mit 3 verschiedenen Datenentitäten',
)

### set here source informations
plot_obj.set_source_info_text(
    text='Hier würden die Angaben zur Quelle stehen',
)

### set here the footnote of the figure
plot_obj.set_footnote_text(
    text=f'\xa9 Umweltbundesamt, {date.today().year}',
)
```

<br>
<br>

We set the axes labels, order the legend by the ```mk_legend()```-method and set the UBA stripes by calling the ````set_stripes()```-method

```python
ax.set_xlabel('X')
ax.set_ylabel('Y')


plot_obj.mk_legend()


### set program to plot the UBA stripes
plot_obj.set_stripes()
```

<br>
<br>

We call the ```show()```-method to get the plot as new window and clean up the working directory of the generated mplstylesheet


```python
### make the plot
plt.show()


### we remove the temporary stylesheet
style_sheet_manager.StyleSheetManager().clean_up()
```
