<i>Keywords:<br>#donut plot; #donut chart;</i><br><br>

This example is about a p chart created with UBAPyPlot to plot those in the UBA Corporate Design. 
The workflow is very similar to that of creating pie charts, with one small difference.
<br>
<br>
An example can be seen below:
<br>
<br>
<center>
	<img src="_static/donut.png" style="background-color:white;" alt="Figure shows a donut chart created with UBAPyPLot" width="50%"/>
</center>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 20
		</b>
		<br>
		<br>
		A donut chart created by UBAPyPlot.<br>
	</p>
</div>
<br>
<br>
<br>
<br>

<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>


We load the necessary packages that we need.
<br>

``` python

'''import external packages'''
import matplotlib.pyplot as plt
from matplotlib.patches import Patch

'''import the UBAPyPlot-Package'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```

<br>
<br>

We set the Matplotlib Plot Style to an own costumized style sheet.
<br>

``` python
### we set here the plot style to the UBA own costumized style sheet
### corresponding to a standard chart plot
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('standard_chart'))
```

<br>
<br>

We instantiate an UBA_plot class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual.
We change some class attributes by passing their new definitions to the class constructor.
<br>
Those as:

* Size of the Plot
* Height of the Child Axes object
* and the BBOX settings

<br>

``` python

plot_obj = UBA_Plot.UBA_Plot(size=(4, 6), height_child= '80%', bbox_to_anchor=(-0.06, 0, 1.1, .95))

ax       = plot_obj.get_child_ax()
```

<br>
<br>

We create some dummy data directly within the plot method ```pie()``` that we can directly apply to the axes object.
<b>We use the ```pie()```-method although we will create a donut diagram.</b>

```py
wedges, text, autotext = ax.pie(
    # The data what we want to plot
    [25, 40, 56, 5], 

    # color definition of each wedge
    colors=[
		(0/256, 118/256, 38/256),
		(0/256, 155/256, 213/256), 
		(98/256, 47/256, 99/256), 
		(131/256, 5/256, 60/255)
	],

    # explode factor of each wedge | 0 means no shift, 0< means a shift outside from the center of pie
    explode=[0.07, 0.0, 0.0, 0.09],
    
    # determines the distance from the center were the label should be plotted
    pctdistance=0.83,

    # kind of how to show the percentage numerical label
    autopct='%0.1f%%',

    # some style configurations of the wedges
    wedgeprops = {
        'linewidth' : .6,
        'edgecolor' : 'w'
    },
    
    # the radius of the pie !!!may not be larger than 1!!!
    radius=.9,
   
    # text properties
    textprops={
        'fontsize': 7,
        'color'   : 'w'
        },

    # sets the center of the pie
    center=(0, 3)
    )
```

<br>
<br>

Now we use the ```setp()```-method of PyPlot to obtain the donut instead of the pie

```py
# to obtain a pie chart insteat of a donut chart do just comment out the line below    
plt.setp(wedges, width=0.3)  # the with parameter determines the the width of the wedge (inner radius)
```


<br>
<br>

We set the UBA stripes 

```py
### plot the UBA stripes
plot_obj.set_stripes()
```

<br>
<br>

We set the text element definitions

```py
### set here the figure number text
plot_obj.set_fig_number_text(
    text=u'Exemplarische Abbildung \u2192 nicht Bestandteil einer öffentlichen Publikation des UBA',
    fontsize=6
)

### set here the title of the figure
plot_obj.set_title_text(
    text='Beispiel eines Tortendiagramms',
)

### set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='Die Datengrundlage ist fiktiv',
)

### set here source informations
plot_obj.set_source_info_text(
    text='Hier würden die Angaben zu den Quellen stehen',
)

### set here the footnote of the figure
plot_obj.set_footnote_text(
    text='\xa9 Umweltbundesamt, 2023',
)
```

<br>
<br>

We call the ```mk_legend()```-method and pass some optional parameters since we have to do in that case some special

```py
plot_obj.mk_legend(
    ncol=3, 
    shift_fac=1, 
    custom_handles=(
        Patch(
            facecolor=(0/256, 118/256, 38/256), 
            label='Share of A'
        ),
        Patch(
            facecolor=(0/256, 155/256, 213/256), 
            label='Share of B'
        ),
        Patch(
            facecolor=(98/256, 47/256, 99/256), 
            label='Share of C'
        ),        
        Patch(
            facecolor=(131/256, 5/256, 60/255), 
            label='remaining Share'
        ),               
    )
)
```

<br>
<br>

We clean up the working directory of the created mplstylesheet and call the ```show()```-method to obtain the plot as new window

```py
### we remove the temporary stylesheet
style_sheet_manager.StyleSheetManager().clean_up()

plt.show()
```


