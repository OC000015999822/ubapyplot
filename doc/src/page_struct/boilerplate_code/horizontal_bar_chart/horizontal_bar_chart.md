<i>Keywords:<br>#Bar Chart; #Vertical Bar Chart; #Balkendiagramm; #vertikales Balkendiagramm</i><br><br>

This example is about a vertical Bar Chart with UBAPyPlot to plot those in the UBA Corporate Design. 
<br>
<br>
An example can be seen below:
<br>
<br>
<img src="_static/horizontal_bar_chart.png" style="background-color:white;" alt="Figure shows a verticla bar chart created with UBAPyPLot"/>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 17
		</b>
		<br>
		<br>
		A horizontal bar chart created by UBAPyPlot.<br>
	</p>
</div>
<br>
<br>
<br>
<br>




<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>



We load the necessary packages that we need.
<br>

```python

'''import external packages'''
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.colors import LinearSegmentedColormap
from datetime import date


'''import the UBAPyPlot-Package'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```

<br>
<br>

We set the Matplotlib Plot Style to an own UBA costumized style sheet. Since we want to plot a vertical bar chart we set the ```style_kind``` parameter to ```v_bar_chart```.
<br>

```python
### we set here the plot style to the UBA own costumized style sheet for creating vertical bar charts
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('v_bar_chart'))
```

<br>
<br>

We instantiate an <a href="https://oc000015999822.usercontent.opencode.de/ubapyplot/doc/src/_build/#UBAPyPlot.UBA_Plot.UBA_Plot">UBA_plot</a> class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual with PyPlot.
<br>

```python

### do instatiate the UBA_CD_PyPlot object
### we set some class attributes to get an better look of the plot figure
plot_obj = UBA_Plot.UBA_Plot(height_child='55%', width_child='73%', bbox_to_anchor=(0.12, 0.03, 1, 1))

### grab the child axes object
ax       = plot_obj.get_child_ax()
```

<br>
<br>

<br>
<br>

We create a dataset based on population data of the german federal countries.<br>
We want to get a bar chart with the population number of each country as X-dimension (bar length) and the corresponding country as name maped to the Y-axis.<br>
*The data that we use came from Wikipedia: <a href="https://de.wikipedia.org/wiki/Land_(Deutschland)">https://de.wikipedia.org/wiki/Land_(Deutschland)</a>. See the table named "Politik"*
<br>

```python
### federal countries
bl = [
    r'$Baden-Württemberg$',
    r'$Bayern$',
    r'$Berlin$',
    r'$Brandenburg$',
    r'$Bremen$',
    r'$Hamburg$',
    r'$Hessen$',
    r'$Mecklenburg-Vorpommern$',
    r'$Niedersachsen$',
    r'$Nordrhein-Westfalen$',
    r'$Rheinland-Pfalz$',
    r'$Saarland$',
    r'$Sachsen$',
    r'$Sachsen-Anhalt$',
    r'$Schleswig-Holstein$',
    r'$Thüringen$'
]


### population number of the federal contry (corresponds to the list above)
p_n = [
    11.103,
    13.140,
    3.664,
    2.531,
    0.680,
    1.852,
    6.293,
    1.611,
    8.003,
    17.926,
    4.098,
    0.984,
    4.057,
    2.181,
    2.911,
    2.120
]




### we instantiate a PandasDataFrame an add the lists abova as columns
df = pd.DataFrame(
    {
        'federal_country_state' : bl,
        'number_of_people'      : p_n
    }
)
```

<br>
<br>

We sort the data within the DataFrame in descanding wise to get a nice plot
<br>

```python
### we sort the DataFrame by population size in descending order
df = df.sort_values(by=['number_of_people'])
```

<br>
<br>

We create a numerical proxy as index axis that represents the federal contry names in numerical wise

```python
### we instantiate a numerical proxy for the Y-Axis that represent the federal country definition
y_pos = np.arange(len(bl))
```

<br>
<br>

We  create a color map list to get a nice plot style with color shades regarding the population number
<br>

```python
### we define the color range (we use UBA Corporate Identity compliant colors)
colors = [(0/256, 118/256, 38/256), (0/256, 95/256, 133/256)]


### we create a color map of shades between the both colors created above
cmap = LinearSegmentedColormap.from_list('color_list', colors, N=len(df))


### we list the color map as mpl color list object
colors = cmap(np.arange(0,cmap.N)) 
```

<br>
<br>

We do the plot 
<br>

```python
ax.barh(y_pos, df.number_of_people.values, align='center', color=colors)
```

<br>
<br>

We set the list of the federal country names as Y-tick labels and set the X-axis label

```python 
### we set here the federal contry names as y-ticks
ax.set_yticks(y_pos, labels=df.federal_country_state.values)

### we set the x-axis label
ax.set_xlabel(r'$Einwohner*innen~(Mio)$')
```

<br>
<br>

We set all text elements within the parent axis object

```python
### set here the figure number text
plot_obj.set_fig_number_text(
    text=u'Exemplarische Abbildung \u2192 nicht Bestandteil einer öffentlichen Publikation des UBA',
)

### set here the title of the figure
plot_obj.set_title_text(
    text='Einwohner*innen-Anzahl pro Bundesland',
)

### set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='Stand: 31. Dezember 2020',
)

### set here source informations
plot_obj.set_source_info_text(
    text='Daten der statistischen Ämter von Bund und Länder.\nEntnommen aus Wikipedia (https://de.wikipedia.org/wiki/Land_(Deutschland)); 08.02.2023.',
)

### set here the footnote of the figure
plot_obj.set_footnote_text(
    text=f'\xa9 Umweltbundesamt, {date.today().year}',
)
```

We the ranges of the y- and x-dimension

```python

ax.set_xlim([-.5, 20.1])

ax.set_ylim([-1, 17])
```

<br>
<br>

We want to get a fancy vertical gridline set
Each 5'th grid line should be fat and as whole line and all others dotted lines
We also want to get numerical x-tick labels but only at each 5'th tick

```python
x_tick_loc = range(21)

x_tick_lab = []
for x in x_tick_loc:
    if x % 5 == 0:
        x_tick_lab.append(x)
        ax.axvline(x=x, color = "grey", lw=.5, ymax=.9, zorder=0) 
        
        
    else:
        x_tick_lab.append(None)

    ax.axvline(x=x, color = "grey", ls=':', lw=.5, ymax=.9, zorder=0)


### we set here the x-tick labels (only each 5'th should be appear)
ax.set_xticks(x_tick_loc)
ax.set_xticklabels(x_tick_lab, minor=False)
```

<br>
<br>

we hide the left and bottom spline to get a nice plot
<br>

```python
ax.spines.left.set_visible(False)
ax.spines.bottom.set_visible(False)
```

<br>
<br>

We set the UBA stripes

```python
### set program to plot the UBA stripes
plot_obj.set_stripes()
```

<br>
<br>

We call the ```plt.show()```-method to see the plot and remove the mplstyle sheet template

```python
### make the plot
plt.show()


### we remove the temporary stylesheet
style_sheet_manager.StyleSheetManager().clean_up()
```