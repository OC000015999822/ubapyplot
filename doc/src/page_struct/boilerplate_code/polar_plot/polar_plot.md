<i>Keywords:<br>#polar plot; #polar chart; #polar coordinates; #windrose </i><br><br>

This example is about a polar chart created with UBAPyPlot to plot those in the UBA Corporate Design. 
<br>
<br>
An example can be seen below:
<br>
<br>
<center>
	<img src="_static/polar.png" style="background-color:white;" alt="Figure shows a polar chart created with UBAPyPLot" width="50%"/>
</center>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 21
		</b>
		<br>
		<br>
		A polar chart created by UBAPyPlot.<br>
	</p>
</div>
<br>
<br>
<br>
<br>

<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>


We load the necessary packages that we need.
<br>

```python

'''import external packages'''
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib import pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.cm as cm

'''import the UBAPyPlot-Package'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```

<br>
<br>

We set the Matplotlib Plot Style to an own costumized style sheet.
<br>

```python
### we set here the plot style to the UBA own costumized style sheet
### corresponding to a standard chart plot
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('standard_chart'))
```

<br>
<br>

We instantiate an UBA_plot class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual.
We change some class attributes by passing their new definitions to the class constructor.
<br>
Those as:

* Size of the Plot
* Height of the Child Axes object
* BBOX settings
* and the projection sheme that in this case is <u><b>polar</b></u>


<br>

```python

plot_obj = UBA_Plot.UBA_Plot(size=(4, 6), height_child= '50%', projection='polar', bbox_to_anchor=(-0.05, 0, 1.1, .95))

ax       = plot_obj.get_child_ax()
```

<br>
<br>

We create some dummy data 

```python
N = 20                                                  # number of entities
theta = np.linspace(0.0, 2 * np.pi, N, endpoint=False)  # angle of attack
radii = 10 * np.random.rand(N)                          # list of radiies
width = np.pi / 4 * np.random.rand(N)                   # the with of each entity
```

<br>
<br>

We create a customized color map

```python
colors = [
	(0/256, 118/256, 38/256), 
	(0/256, 95/256, 133/256), 
	(157/256, 87/256, 154/256), 
	(131/256, 5/256, 60/256)
	] 
	
cmap_name = 'my_list'

cmap = LinearSegmentedColormap.from_list(cmap_name, colors, N=20)


colors = cmap                    # colors for each

colors = cmap(np.arange(0,cmap.N)) 
```

We do the plot by using the ```bar()-method``` onto the axes object.
Since we setted the coordinate system to polar the bar plot is automaticly transformed into a pola coordinates system

```python
ax.bar(theta, radii, width=width, bottom=0.0, color=colors, alpha=0.5)
```

<br>
<br>

We set the UBA stripes by calling the ````set_stripes()```-method onto the axes object

```python
### set program to plot the UBA stripes
plot_obj.set_stripes()
```

<br>
<br>

We set the text element definitions

```python
### set here the figure number text
plot_obj.set_fig_number_text(
    text=u'Exemplarische Abbildung \u2192 nicht Bestandteil einer öffentlichen Publikation des UBA',
    fontsize=6.3
)


### set here the title of the figure
plot_obj.set_title_text(
    text='Beispiel eines Polardiagramms',
)

### set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='Die Datenbasis ist fiktiv',
)

### set here source informations
plot_obj.set_source_info_text(
    text='Hier würden die Angaben zu den Quellen stehen',
)

### set here the footnote of the figure
plot_obj.set_footnote_text(
    text='\xa9 Umweltbundesamt, 2023',
)
```

<br>
<br>

We call the ```mk_legend()```-method and pass some optional parameters since we have to do in that case some special

```python
plot_obj.mk_legend( 
    shift_fac=1.2, 
    fontsize=6,
    custom_handles=(
        Patch(
            facecolor='#007626', 
            #edgecolor='w',
            label='2017'
        ),
        Patch(
            facecolor='#005f85', 
            #edgecolor='w',
            label='2018'
        ),
        Patch(
            facecolor='#622f63', 
            #edgecolor='w',
            label='2019'
        ),        
        Patch(
            facecolor='#83053c', 
            #edgecolor='w',
            label='2020'
        ),               
    )
)
```

<br>
<br>

We clean up the working directory of the created mplstylesheet and call the ```show()```-method to obtain the plot as new window

```python
### we remove the temporary stylesheet
style_sheet_manager.StyleSheetManager().clean_up()

plt.show()
```


