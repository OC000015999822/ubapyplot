The basic technical concept is to represent plot figures by two complementary objects. These objects are concretizations of *PyPlot* axes objects. 
So they can be handled exactly according to their scheme as known when working with *PyPlot* axes objects. The already known workflow remains the same and is only extended by some but less further steps.

The first axes object is the so-called `Parent Axes Object`. This Parent Axes Object contains all basic style elements which define the typically UBA Corporate Design those as the 4 horizontal style lines, 
the text elements (figure number, plot title, plot sub-title, footnote and source information) and the area of the grey hatch.

The second axes object is the so-called Child Axes Object. This axes object is automaticly embedded in the corresponding `Parent Axes Object` in the grey hatched area and contains the base plot object or 
diagram to be mapped.

To modify/set the *UBA CD* style elements contained in the Parent Axes Object, for each of these style elements a setter-method is programmed and can be used. 
There are some use case examples in the chapter [Boilerplate-Code/Examples 8](#boilerplate-code-examples-8) to see the functionality in practical wise. The principle is verry intuitive.

The figure below visualizes the basic concept of the cooperation of the two different axes objects which combine the whole figure:



<br>
<br>
<br>

<img src="_static/principle.png" alt="Figure shows the principle of how UBA figures get created by the workflow. It shows all elements such as the parent axes object area as well as the child axes object area. Furthermore, the diferent textelements and their locations are shown."/>

<div class="fig_capt">
	<p>
		<b>Fig. Nr.: 2</b><br><br>
		Figure shows the principle of how UBA figures get created by the workflow. It shows all elements such as the parent axes object area as well as the child axes object area.<br>
		Furthermore, the diferent textelements and their locations are shown.<br>
		It shows the basic elements and their atrubutations of a plot figure in UBA Corporate Design. The link to the document can be found in the upper part of this subchapter.
	</p>
</div>


<hr style="height: 5px; width: 100%; color: grey;">

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>