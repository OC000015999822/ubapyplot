<i>Keywords:<br>#bubble plot; #bubble chart; #multi dimension plot</i><br><br>

This example is about a bubble chart created with UBAPyPlot to plot those in the UBA Corporate Design. 
<br>
<br>
An example can be seen below:
<br>
<br>
<center>
	<img src="_static/bubble.png" style="background-color:white;" alt="Figure shows a bubble chart created with UBAPyPLot" width="50%"/>
</center>

<div class="fig_capt">
	<p>
		<b>
			Fig. Nr.: 22
		</b>
		<br>
		<br>
		A bubble chart created by UBAPyPlot.<br>
	</p>
</div>
<br>
<br>
<br>
<br>

<p style="font-size: 25px;"><i><u>Code:</u></i></p>
<br>
<br>

We load the necessary packages that we need.
<br>

```python

'''import external packages'''
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.colors import LinearSegmentedColormap
from datetime import date
import random as rand


'''import the UBAPyPlot-Package'''
from UBAPyPlot import UBA_Plot
from UBAPyPlot import style_sheet_manager
```

<br>
<br>

We set the Matplotlib Plot Style to an own costumized style sheet.
<br>

```python
### we set here the plot style to the UBA own costumized style sheet
### corresponding to a standard chart plot
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('standard_chart'))
```

<br>
<br>

We instantiate an UBA_plot class object and grab the child axes object by the ```get_child_ax()```-method. The returned child axes object now can be treated as usual.
We change some class attributes by passing their new definitions to the class constructor.
<br>
Those as:

* Size of the Plot
* Height of the Child Axes object
* BBOX settings
* and the ```y_line_1```


<br>

```python

plot_obj = UBA_Plot.UBA_Plot(
	size=(5, 6), 
	height_child='53%', 
	width_child='80%', 
	bbox_to_anchor=(0.04, 0.01, 1, 1), 
	y_line_1=0.03
	)

ax       = plot_obj.get_child_ax()
```

<br>
<br>

We define a exponential function to get a fancy plot an define some dummy data

```python
def exp_fun(x):
    return 50*np.exp(-x*.06) -10



x = np.arange(50)
y = [exp_fun(x_) + rand.gauss(0, 4) for x_ in x]
s = np.random.randint(0,200,50)
c = np.random.randint(0,50,50)
``` 

<br>
<br>

We create a customized color map

```python
colors = [(0/256, 95/256, 133/256), (215/256, 132/256, 0/256)]

cmap_name = 'my_list'

cmap = LinearSegmentedColormap.from_list(cmap_name, colors, N=100)

colors = cmap(np.arange(0,cmap.N)) 
```

<br>
<br>

We do the plot and map it to variable to make later a color map based on it.
We do a second plot that will represent an edge of each bubble

```python
### mk bubbles and map the plot object to a variable
ax_obj = ax.scatter(x,y, s=s, c=c, cmap=cmap, alpha=0.6, edgecolor='None')

### edges in grey
ax.scatter(x,y, s=s, cmap=None, facecolors="None", edgecolor='#4b4b4d', linewidth=.8)
```

<br>
<br>+

We set the UBA stripes by calling the ````set_stripes()```-method onto the axes object

```python
### set program to plot the UBA stripes
plot_obj.set_stripes()
```

<br>
<br>

We set the text element definitions

```python
### set here the figure number text
plot_obj.set_fig_number_text(
    text=u'Exemplarische Abbildung \u2192 nicht Bestandteil einer öffentlichen Publikation des UBA',
)

### set here the title of the figure
plot_obj.set_title_text(
    text='Bubble Chart',
)

### set here the subtitle of the figure
plot_obj.set_subtitle_text(
    text='To visualize 4 dimensions in a simple graphic',
)

### set here source informations
plot_obj.set_source_info_text(
    text='Bubble Plot to visualize a 4-dimensional dataset within a 2-dimensional projection.\nThe X- and Y-dimension is plotted by a cartesian wise, whereas the Z-dimension\nis mapped by to the bubble size and the W-dimension to a color shade.',
    y_shift=-.04
)

### set here the footnote of the figure
plot_obj.set_footnote_text(
    text=f'\xa9 Umweltbundesamt, {date.today().year}',
    y_shift=-.003
)
```

<br>
<br>

We create the legend element within the plot that only reprent the dimension that is mapped by the bubble size.
<b>We do not use the ```mk_legend```-method of the UBAPyPlot framework, since, it wont work for this case. The legend element that we do create here is an extra item.</b>


```python

### we create 3 classes of bubbles tha represent the bubbles as leged items
gll = plt.scatter([],[], s=2, marker='o', edgecolor='#4b4b4d', facecolor='#f0f1f1')
gl = plt.scatter([],[], s=100, marker='o', edgecolor='#4b4b4d', facecolor='#f0f1f1')
ga = plt.scatter([],[], s=200, marker='o', edgecolor='#4b4b4d', facecolor='#f0f1f1')


### we create the legend element
legend = plt.legend((gll,gl,ga),
       ('1%', '10%', '100%'),
       scatterpoints=1,
       loc='upper right',
       ncol=1,
       fontsize=6,
       facecolor='white',
       handletextpad=2,
       labelspacing = 1.5,
       borderpad=1,

       )

### we set here a title of the legend element to show user what dimension is mapped on it
legend.set_title('Feature Z',prop={'size':6})

### some styling of the legend item
legend.get_frame().set_alpha(None)
legend.get_frame().set_facecolor((0, 0, 0, .1))

```

<br>
<br>

We create a color map based on the ```ax_obj``` variable.

```python
cbar = plt.colorbar(ax_obj, orientation='horizontal', label='Feature W')
```

<br>
<br>

We do some style settings...

```python
ax.set_xlabel('Feature X')
ax.set_ylabel('Feature Y')

ax.set_xlim([-5, 55])
ax.set_ylim([-5, 55])

ax.spines.left.set_bounds((0, 50))
ax.spines.left.set_visible(True)
ax.set_yticks([0, 25, 50])

ax.spines.bottom.set_bounds((0, 50))
ax.set_xticks(np.arange(0, 60, 10))

### we hide the x ticks (but keep the labels)
ax.tick_params(axis='x', which='both',length=0)

ax.set_yticks(np.arange(0, 60, 10))
```

We clean up the working directory of the created mplstylesheet and call the ```show()```-method to obtain the plot as new window

```python
### we remove the temporary stylesheet
style_sheet_manager.StyleSheetManager().clean_up()

plt.show()
```


