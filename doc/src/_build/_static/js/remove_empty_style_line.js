/* 
To remove the style line below the empty header and section (first section that is used as entry point 


*/

function remove_empty_style_line() {	
	// selct all h tag elements
	var h_tags = document.querySelectorAll("h1, h2, h3, h4, h5, h6");
	
	// iterate over the h tag elements to check them for the specific one
	for (var i=0; i < h_tags.length; i++) {
	
		// the wanted h tag has no inner text
		if (h_tags[i].innerText === '') {
		
			// we remove the element
			h_tags[i].remove();	
			
			
			// we remove the 
		}
	}
}

remove_empty_style_line();