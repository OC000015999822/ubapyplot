	
/*
This script is to manage the sidebar funtion in respect to the window width.
It aims not to the ussage of mobile devices but to decrease the window size
if necassary.

By Alexander Prinz | 28.07.2023
*/


function sidebarManager() {
	window.addEventListener('resize', () => {
	
		// we grab the sidebar node element 
		var sidebar = document.getElementById('right_sidebar');


		// we evaluate if the screen width is larger than 800 pixels
		if (window.innerWidth > 800) {  //CASE: User either use a smartphone or has decreased the window size 
		
		
			// we set the display attribute to block to make the sidebar visible
			sidebar.style.display = 'block';
			

			// we remove the event listeners of each of the links if those are effected 
			var sidebarLinks = document.querySelectorAll('#right_sidebar a');
			sidebarLinks.forEach(link => {
				link.replaceWith(link.cloneNode(true));	
			});

					
		} else {                       //CASE: User does not use a mobile device or has encreased the windowsize
			// we do set the sidebar to invisible in general when the screen width is smaller than 800
			sidebar.style.display = 'none'; 
			
			
			// ...but after clicking the toggle sidebar button to make sidebar appear
			// we want the sidebar disappear after clicking a link inside and do iterate accross all its links
			// and assign an click event to it that make the sidebar dissappear after clicking it
			var sidebarToggleBtn = document.getElementById('toggle_right_sidebar');
					
			sidebarToggleBtn.addEventListener('click', () => {
				
				var sidebarLinks = document.querySelectorAll('#right_sidebar a'); 
								
				sidebarLinks.forEach(link => {
					link.addEventListener('click', () => {
						sidebar.style.display = 'none';
					});				
				});	
			});
		}
	});
}

sidebarManager();