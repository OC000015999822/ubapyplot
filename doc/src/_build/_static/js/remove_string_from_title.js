/* 
For subpages sphinx add sensless string to the title.
We remove them by this script 

By: Alexander Prinz

*/

function remove_string_from_title() {
	// fetch the current title of subpage 
	var current_title = document.title;
	
	// replace the substring
	var new_title = current_title.replace("<no title>", '');
	
	// change title by altered title string
	document.title = new_title;
}

remove_string_from_title();