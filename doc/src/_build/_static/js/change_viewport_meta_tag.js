/*
We use this JS to overwrite the viewport meta tag to prevent sliding the scren in horizontal wise when
using a mobile device.

By: Alexander Prinz | 28.07.2023

*/

function change_viewport_meta_tag() {
	
	var view_port_meta_tag = document.querySelector('meta[name="viewport"]');
	view_port_meta_tag.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no');
}

change_viewport_meta_tag();