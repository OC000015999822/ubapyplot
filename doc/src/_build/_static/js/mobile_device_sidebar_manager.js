/*
We use this script to close the sidebar automaticly when the user has pushed a link button 
within the sidebar if user has called the app with a mobile device.
Script is only activated when user does definitely use a mobile device.
*/

function mobileDeviceSidebarManager() {
	if (window.innerWidth <= 800) {

		// function that closes the sidebar by setting its style attribute <display> to <none>
		function closeSidebar() {
			const sidebar = document.getElementById('right_sidebar');
			sidebar.style.display = 'none';		
		}
		
		
		// we iterate accross all links within the sidebar and add an eventlistener to them 
		// that calls the closeSidebar() function when clicking on any link
		const sidebarLinks = document.querySelectorAll('#right_sidebar a');
		
		sidebarLinks.forEach(link => {
			link.addEventListener('click', () => {
			closeSidebar();	
			});
		});
	} 
}

mobileDeviceSidebarManager();