/*
We use this script to hide the toggle sidebar button when user is on a subpage 
caused by the search bar result.
Insteat, we offer a back to home button.

By Alexander Prinz | 28.07.2023

*/


/* function to evaluate if the user is on the index page or a subpage */
function isOnHomePage() {

	if (!window.location.pathname.includes('_modules') & (window.location.pathname.endsWith('/') || window.location.pathname.includes('/index.html'))) {
		return true;
	} else {
		return false;
	}
}



/* function to evaluate whether the source of the page is a (netwaork)drive or http(s) service */
function isSrcHTTP() {
	if (window.location.protocol === "file:") {
		return false;
	} else {
		return true;
	} 
} 


// main function of this script
function manageLinks() {
	
	
	const path = window.location.pathname;	
	
	const directories = path.split("/");
	
	if (isOnHomePage()) {
	
		// we remove the back to homepage button if the user is on the index page
		document.getElementById("back-to-directory").remove();
	
		console.log("CHECK: index page");
		console.log(window.location.pathname);
	
	} else {
		console.log("CHECK: subpage");
	
		// we get the toggle the right side bar link
		const toggle_link = document.querySelector('.toggle_right_sidebar');
	
	
	
		// CASE: we are in the search-page
		if (window.location.pathname.includes('/search.html')) {		
		
			console.log("CHECK: subpage but in root");
		
		
			// we locate the index.html file within the path to enshure to link the index.html to the <back to home button>
			// we create the path to the index file
			const directories            = path.split("/");			
			const path_to_index_html     = directories.slice(0, directories.length-1).join('/') + '/index.html';
			
			// we create the back to home link with an icon 
			const back_to_home_navi_link = document.createElement('a');
			back_to_home_navi_link.href      = path_to_index_html;
			back_to_home_navi_link.innerHTML = '<img src="_static/home_mobile_icon.png" style="filter: invert(100%); padding: 0 10px 0 0; margin-top: 2px;" height="20px" width="20px" />';
			back_to_home_navi_link.className = 'back_to_home_mobile_navi';
			
			
			// we do replace the link		
			toggle_link.parentNode.replaceChild(back_to_home_navi_link, toggle_link);
			
					
		
		
		// CASE: we are in a subpage located in subdirectories relative to root of <index.html>
		} else {
		
			console.log("CHECK: subpage in subdir");

		
			// we do evaluate whether the page is requested from a webserver or a drive
			if (isSrcHTTP()) {
				console.log("CHECK: page was responsed from HTTP(S) server");
				
				

				
				
			
				// we locate the index.html file within the path to enshure to link the index.html to the <back to home button>
				const directories = path.split("/");	
				
				var go_back_path_suffix = '';

				for (i = 1; i < directories.length; i++) {
					
					
					//make path of the i'th directory of this iteration process
					var indexPath = directories.slice(0, directories.length - i).join("/");
						
						

					
					// instantiate a HTTPRequest object
					var xhr = new XMLHttpRequest();
					
					// add the current iteration path to http header
					xhr.open('HEAD', indexPath + '/index.html', false);
					

					// make request to webserver
					xhr.send();
					


		
					/*we evaluate the server response.
					For case, the server do answer with 200, it will indicates that the webpage is valid and 
					the index.html is located in that path that the current url mappes on.
					Otherwise - response is 404 or 4xy - we know, that the page is invalid and we are not in the valid path.
					We do continue the iteration if the HTTP respont was 200 but the directory is '_modules', since that indicates the index.html is not the 
					main page.
					*/
					if ((xhr.status === 200) & (indexPath.split('/').at(-1) != "_modules")) {					
						var indexUrl = indexPath + '/index.html';																			
						break;
					}
				}
				

			} else {
				console.log("CHECK: page was responsed from a (network)drive");
				
				for (i = 1; i < directories.length; i++) {
					
					
					//make path of the i'th directory of this iteration process
					var indexPath = directories.slice(0, directories.length - i).join("/");

					// evaluation whether directory name is <_build> that would be indicating the root dir of the index.html
					if (indexPath.split("/").slice(-1)[0] === "_build") {		

						// we create the url/path to the directory where the index.html is located
						indexUrl = indexPath + '/index.html';
						break;
					}
				}						
			}
			
					

					
			// we create an a-tag that is going to represent the <back to home>
			const back_to_home_navi_link = document.createElement('a');

			back_to_home_navi_link.href = indexUrl;
			back_to_home_navi_link.innerHTML = '<img src="' + indexPath + '/_static/home_mobile_icon.png" style="filter: invert(100%); padding: 0 10px 0 0; margin-top: 2px;" height="20px" width="20px" />';
			back_to_home_navi_link.className = 'back_to_home_mobile_navi_in_sub_dir_page_new';
			
			
			// we replace the placeholder
			document.getElementsByClassName("back_to_home_mobile_navi_in_sub_dir_page_placeholder")[0].replaceWith(back_to_home_navi_link);
		}
		
	// we remove the toggle right sidebar link since we do not have a sidebar within subpages
	document.getElementsByClassName("toggle_right_sidebar")[0].remove();
	}
}

// we call the main function
manageLinks();