/* ********************************************************************************************************************************** */

/* JS to have a dynamic link regarding the back to main page button.
It enshures that no matter which subpage the user has opend the back_to_main_page_button points to _build-directory
Or, if the page is deployed on a real webserver, the back_to_main_page_button will than point to the index page.
The same for images. */

/* ********************************************************************************************************************************** */


function fun() {

	// get path of this site as string
	const path = window.location.pathname;
	
	// list all path entities of the path string 
	const directories = path.split("/");
	

		
	// check wether the <_build> directory is in the list (indicates, that page is deployed on the UBA drive and on by a webserver)
	if (directories.includes("_build") ) {
	
		// get the index of the list element <_build> that indicates the page root directory
		const index = directories.indexOf("_build");
		
		// rebuild the path from glabal root to the entry path of the web page (here <_build>)
		const directory = directories.slice(0, index + 1).join("/");		

		// set the href attribute of the respective anchor tag of the back_to_main_page_button
		document.getElementById("back-to-directory").href = directory + '/index.html';	

		// set the src attribute of the UBA logo image to the correct one
		document.getElementById("logo").src = directory + '/_static/uba_logo.png';
						
		
								
		/* ************************************************************************************** */		
		/* *********************** We iterate across all image tags ***************************** */
		/* ************************************************************************************** */
		
		// get list of all image tags
		const img_tag_list = document.getElementsByTagName('img');
		
		<!-- console.log(img_tag_list); -->
		
		// do iterate across the image tags
		for (var i=0; i < img_tag_list.length; i++) {
		
			// specifig image tag that fulfill filter criterion by the id="img"
			var img_tag = img_tag_list[i];

			// get img name from the src-attribute
			var img_name = img_tag.src.slice(img_tag.src.lastIndexOf("/") + 1);
			
			// overwite the src-attribute by the correct url
			img_tag.src = directory + '/_static/' + img_name;
		}				
						
		
	} else {
	
		document.getElementById("back-to-directory").href = '/index.html';
	
		// the sphinx page is deployed on a webserver, so we have to fall back to local root of the page indicated by the index.html
				
		// get the url of current location
		var currentUrl = window.location.href;

		
		var pathName = window.location.pathname;
		
		var index = pathName.lastIndexOf('/');
		
		
		
		/* ********************************************************************************************************* */
		/*We do iterate across all subdirectories backward to the global root of the webserver until the directory
		where the index.html file is located. That points us the local root directory of the webpage */
		/* ********************************************************************************************************* */
		for (i = 1; i < directories.length; i++) {
			
			
			//make path of the i'th directory of this iteration process
			var indexPath = directories.slice(0, directories.length - i).join("/");
			

			// instantiate a HTTPRequest object
			var xhr = new XMLHttpRequest();
			
			// add the current iteration path to http header
			xhr.open('HEAD', indexPath + '/index.html', false);

			// make request to webserver
			xhr.send();

	
			/*we evaluate the server response.
			For case, the server do answer with 200, it will indicates that the webpage is valid and 
			the index.html is located in that path that the current url mappes on.
			Otherwise - response is 404 or 4xy - we know, that the page is invalid and we are not in the valid path.*/
			if ((xhr.status === 200) & (indexPath.split('/').at(-1) != "_modules")) {
			
				var indexUrl = indexPath + '/index.html';

				// change the url of the back_to_main_page_button to the path where the index.html is located (root path of the website)
				document.getElementById("back-to-directory").href = indexUrl;
				
				// change the url of the UBA-logo item to the _static-directory location within the root dir of this web page
				document.getElementById("logo").src = indexPath + '/_static/uba_logo.png';
																
				break;
			}
		}


		/* ************************************************************************************** */
		/* *********************** We iterate across all image tags ***************************** */
		/* ************************************************************************************** */
		
		// get list of all image tags
		const img_tag_list = document.getElementsByTagName('img');
		

		
		// do iterate across the image tags
		for (var i=0; i < img_tag_list.length; i++) {
		
		
			// specifig image tag that fulfill filter criterion by the id="img"
			var img_tag = img_tag_list[i];
	
	
			// get img name from the src-attribute
			var img_name = img_tag.src.slice(img_tag.src.lastIndexOf("/") + 1);
			

			// overwite the src-attribute by the correct url
			img_tag.src = indexPath + '/_static/' + img_name;
			

		}
	}
}

fun();