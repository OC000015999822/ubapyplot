/* 
we use this script to add the css style attribute <float: right> to the vavi bar element
to archive that the links/icons keep the right sided location if the screen is small 
(mobile devices)	

By: Alexander Prinz

*/

function set_navibar_links_to_right() {
	var nav_element = document.querySelector('nav');
	
	nav_element.style.float= "right";
}
	
set_navibar_links_to_right();