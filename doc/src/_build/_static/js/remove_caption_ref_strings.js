/* 
We use this JaveScript to remove caption reference strings may included in RMarkdown
within figure caption strings.

By: Alexander Prinz | 28.07.2023

*/

function remove_caption_ref_strings() {
	// get all a tags
	var p_tags = document.getElementsByClassName("caption")

	// do iterate over all a tags and find the one with the specific title that indices the right one
	for (var i = 0; i < p_tags.length; i++) {
				
		// we select those tags wich include the indicator string within the inner text	
		if (p_tags[i].innerText.includes("\#fig:")) {
		
			// we replace the substring that should be removed by an empty string
			p_tags[i].innerText = p_tags[i].innerText.replace(/ *\([^)]*\) */g, "");			
		}
	}
}

remove_caption_ref_strings();