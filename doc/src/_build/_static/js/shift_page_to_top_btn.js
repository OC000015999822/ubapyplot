/* 
We use this script to add a button into the sidebar that shift the page to its top. 
There is allready an entrypoint as a tag induced by the empty MarkDown page that we can use.

By: Alexander Prinz 

*/


function shift_page_to_top_btn() {
	// we extract all a tags within that div container representing the sidebar
	const a_nodes_in_sidebar = document.querySelector(".page_toc").querySelectorAll('a')
	
	// we iterate over all of this a tags to check them for the right one we want to replace
	for (var i=0; a_nodes_in_sidebar.length; i++) {
		
		// get the a tag of the link to the pseudo main page/pseudo title page within the sidebar toc 
		if (a_nodes_in_sidebar[i].getAttribute("class") === 'reference internal' & a_nodes_in_sidebar[i].getAttribute("href") === '#') {
		
			// we replace the a tag by a div container and a button as its child using the outer HTML of the a tag
			a_nodes_in_sidebar[i].outerHTML = '<div style="padding: 0 0 20px 0" class="p-xsm-top p-xsm-bottom p-sm-left"><a href="#" title="go to top of the document"><img class="back_to_page_top_icon" src="_static/back_to_top_icon.png" height="20px" width="20px" title="go to top of the document"/></a></div>';

			break;
		}	
	}
}

shift_page_to_top_btn();