 y      sphinx.addnodesdocument)}(	rawsource children](docutils.nodes	paragraph)}(hX½  *Matplotlib* offers beside to its explizit functionalities with *PyPlot* two different ways to style plot figures.
The key component of these two ways are the so-called RC parameters. 
RC parameters can be used to customize style settings of the plots to get an own individual style. These parameters are
managed by a *style sheet*.
<br>
<br>
Please have a look at *Matplotlib's* web page about cutomizing style sheets and RC parameters at the link below:
<br>
<br>
<a href="https://matplotlib.org/stable/tutorials/introductory/customizing.html">Customizing Matplotlib with style sheets and rcParams</a>
<br>
<br>
<br>
The user can either use a selected style sheet (or default one) that is a ASCII-text file that contains/manages the style parameters (```rcParams```) or overwrites the default rcParams explicit within the *python*-code
by *Matplotib's* functionality to modify the rcParam style sheet as assitioates list (dictionary) that is represented by the ```matplotlib.rcPrams``` object.
<br>
<br>
<br>
The following code snipped is from the original *Matplotlib* web page as an example to explain how to modify the RC Parameters within the ```matplotlib.rcParams``` object (as dictionary):
<br>
<br>
<br>h](h	emphasis)}(h
Matplotlibh]h	Text
Matplotlib}(parenth	_documenthsourceNlineNuba
attributes}(ids]classes]names]dupnames]backrefs]utagnamehhKhRZ:\pub\software\UBAPyPlot\doc\src\page_struct\mpl_style_sheets\mpl_style_sheets.mdhhhhubh4 offers beside to its explizit functionalities with }(hhhhhNhNubh)}(hPyPloth]hPyPlot}(hh2hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhKhh-hhhhubh* two different ways to style plot figures.}(hhhhhNhNubh
}(hhhhhNhNubhDThe key component of these two ways are the so-called RC parameters.}(hhhhhNhNubh
}(hhhhhh-hK ubhwRC parameters can be used to customize style settings of the plots to get an own individual style. These parameters are}(hhhhhNhNubh
}(hhhhhh-hK ubhmanaged by a }(hhhhhNhNubh)}(hstyle sheeth]hstyle sheet}(hh\hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhKhh-hhhhubh.}(hhhhhNhNubh
}(hhhhhh-hK ubh	raw)}(h<br>h]h<br>}(hhthhhNhNubah }(h"]h$]h&]h(]h*]formathtml	xml:spacepreserveuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hhhhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhPlease have a look at }(hhhhhNhNubh)}(hMatplotlib'sh]hMatplotlibβs}(hh‘hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhKhh-hhhhubhL web page about cutomizing style sheets and RC parameters at the link below:}(hhhhhNhNubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hh·hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hhΚhhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(hP<a href="https://matplotlib.org/stable/tutorials/introductory/customizing.html">h]hP<a href="https://matplotlib.org/stable/tutorials/introductory/customizing.html">}(hhέhhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh5Customizing Matplotlib with style sheets and rcParams}(hhhhhNhNubhs)}(h</a>h]h</a>}(hhπhhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hj)  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhThe user can either use a selected style sheet (or default one) that is a ASCII-text file that contains/manages the style parameters (}(hhhhhNhNubh	literal)}(hrcParamsh]hrcParams}(hjB  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKhh-hhhhubh9) or overwrites the default rcParams explicit within the }(hhhhhNhNubh)}(hpythonh]hpython}(hjT  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhKhh-hhhhubh-code}(hhhhhNhNubh
}(hhhhhh-hK ubhby }(hhhhhNhNubh)}(hMatplotib'sh]hMatplotibβs}(hjn  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhKhh-hhhhubhm functionality to modify the rcParam style sheet as assitioates list (dictionary) that is represented by the }(hhhhhNhNubjA  )}(hmatplotlib.rcPramsh]hmatplotlib.rcPrams}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKhh-hhhhubh object.}(hhhhhNhNubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hj©  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hjΌ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubh0The following code snipped is from the original }(hhhhhNhNubh)}(h
Matplotlibh]h
Matplotlib}(hjΣ  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhKhh-hhhhubhN web page as an example to explain how to modify the RC Parameters within the }(hhhhhNhNubjA  )}(hmatplotlib.rcParamsh]hmatplotlib.rcParams}(hjε  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKhh-hhhhubh object (as dictionary):}(hhhhhNhNubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hjϋ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubh
}(hhhhhh-hK ubhs)}(h<br>h]h<br>}(hj!  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKhhhhubeh }(h"]h$]h&]h(]h*]uh,h
hKhh-hhhhubh	literal_block)}(hΫimport numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from cycler import cycler
mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['lines.linestyle'] = '--'
data = np.random.randn(50)
plt.plot(data)
h]hΫimport numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from cycler import cycler
mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['lines.linestyle'] = '--'
data = np.random.randn(50)
plt.plot(data)
}hj8  sbah }(h"]h$]h&]h(]h*]languagepython*-codehhuh,j6  hh-hKhhhhubh)}(hXO	  *The code snipped above represents the workflow how to overwrite explicit (within the runtime) the both style parameters ```lines.linewidth``` (the width of line object within a plot) and*
*```'lines.linestyle'``` (the kind of line that is plotted).*
<br>
<br>
More informations can be seen here:
<br>
<br>
<a href="https://matplotlib.org/stable/tutorials/introductory/customizing.html#runtime-rc-settings">Runtime rc settings</a>
<br>
<br>
<br>
<br>
<br>
If the user has not setted an individual *Matplotlib Style Sheet*, the standard style sheet is loaded and used automaticly.
<br>
<br>
This standard style sheet contains a bunch of default setting regarding all given plot elements like line styles, axes elements such as tickmark setting, labelsetting etc.
<br>
<br>
<br>
To achieve a more individual style in terms of *UBA's* Corporate Design, some *mpl style sheets* have been created for their corresponding use cases.
<br>
They usally should map a bunch of use cases without the need modify them.
<br>
Nethertheless, it is possible to modify them to create an own style. That can be necessary when creating "exotic" plots.
<br>
The user than has the possibility to use either the already descriped ```rcParams``` or to modify the Style Sheet file.
<br>
<br>
The *UBAPyPLot* class ```UBAPyPlot.style_sheet_manager``` (see here: <a href="#UBAPyPlot.style_sheet_manager.StyleSheetManager">UBAPyPlot.style_sheet_manager</a>) includes three
methods to handle the individual UBA Corporate Design associated Style Sheets.
<br>
<br>
There is a setter method to set the corresponding presetted Style Sheet (see here: <a href="#UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style">UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style</a>).
<br>
This method maps an API to *PyPlot's* ```plt.style.use()``` method what is to set the Style Sheet. The method is overloaded by either passing a path to a ```Mpl Style Sheet``` file or pass the name of the listed presetted
plot style that *Matplotlib* delivers together with its python package.
<br>
<br>
When using *UBAPyPlot*, the user now has to pass insteat of the ```UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style()``` method. This passed method has to obtain one of the names of the given UBA Style Sheet
objects.
<br>
<br>
Below, one can see how to set the UBA Style Sheet of vertical bar charts:
<br>
<br>
<br>h](h)}(h΄The code snipped above represents the workflow how to overwrite explicit (within the runtime) the both style parameters lines.linewidth (the width of line object within a plot) andh](hxThe code snipped above represents the workflow how to overwrite explicit (within the runtime) the both style parameters }(hjL  hhhNhNubjA  )}(hlines.linewidthh]hlines.linewidth}(hjT  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hK!hh-hjL  hhubh- (the width of line object within a plot) and}(hjL  hhhNhNubeh }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh
}(hjH  hhhNhNubh)}(h5'lines.linestyle' (the kind of line that is plotted).h](jA  )}(h'lines.linestyle'h]h'lines.linestyle'}(hjt  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hK!hh-hjp  hhubh$ (the kind of line that is plotted).}(hjp  hhhNhNubeh }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj£  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubh#More informations can be seen here:}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjΎ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjΡ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(hd<a href="https://matplotlib.org/stable/tutorials/introductory/customizing.html#runtime-rc-settings">h]hd<a href="https://matplotlib.org/stable/tutorials/introductory/customizing.html#runtime-rc-settings">}(hjδ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubhRuntime rc settings}(hjH  hhhNhNubhs)}(h</a>h]h</a>}(hjχ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj
  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj0  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjC  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjV  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubh)If the user has not setted an individual }(hjH  hhhNhNubh)}(hMatplotlib Style Sheeth]hMatplotlib Style Sheet}(hjm  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh:, the standard style sheet is loaded and used automaticly.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubh«This standard style sheet contains a bunch of default setting regarding all given plot elements like line styles, axes elements such as tickmark setting, labelsetting etc.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj±  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjΔ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjΧ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubh/To achieve a more individual style in terms of }(hjH  hhhNhNubh)}(hUBA'sh]hUBAβs}(hjξ  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh Corporate Design, some }(hjH  hhhNhNubh)}(hmpl style sheetsh]hmpl style sheets}(hj   hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh5 have been created for their corresponding use cases.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhIThey usally should map a bunch of use cases without the need modify them.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj1  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubh|Nethertheless, it is possible to modify them to create an own style. That can be necessary when creating βexoticβ plots.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjL  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhFThe user than has the possibility to use either the already descriped }(hjH  hhhNhNubjA  )}(hrcParamsh]hrcParams}(hjc  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hK!hh-hjH  hhubh# or to modify the Style Sheet file.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjy  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhThe }(hjH  hhhNhNubh)}(h	UBAPyPLoth]h	UBAPyPLot}(hj£  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh class }(hjH  hhhNhNubjA  )}(hUBAPyPlot.style_sheet_managerh]hUBAPyPlot.style_sheet_manager}(hj΅  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hK!hh-hjH  hhubh (see here: }(hjH  hhhNhNubhs)}(h;<a href="#UBAPyPlot.style_sheet_manager.StyleSheetManager">h]h;<a href="#UBAPyPlot.style_sheet_manager.StyleSheetManager">}(hjΗ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubhUBAPyPlot.style_sheet_manager}(hjH  hhhNhNubhs)}(h</a>h]h</a>}(hjΪ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh) includes three}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhNmethods to handle the individual UBA Corporate Design associated Style Sheets.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjω  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhSThere is a setter method to set the corresponding presetted Style Sheet (see here: }(hjH  hhhNhNubhs)}(hE<a href="#UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style">h]hE<a href="#UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style">}(hj#  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh9UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style}(hjH  hhhNhNubhs)}(h</a>h]h</a>}(hj6  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh).}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjM  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhThis method maps an API to }(hjH  hhhNhNubh)}(hPyPlot'sh]h
PyPlotβs}(hjd  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh }(hjH  hhhNhNubjA  )}(hplt.style.use()h]hplt.style.use()}(hjv  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hK!hh-hjH  hhubh_ method what is to set the Style Sheet. The method is overloaded by either passing a path to a }(hjH  hhhNhNubjA  )}(hMpl Style Sheeth]hMpl Style Sheet}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hK!hh-hjH  hhubh. file or pass the name of the listed presetted}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhplot style that }(hjH  hhhNhNubh)}(h
Matplotlibh]h
Matplotlib}(hj’  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh+ delivers together with its python package.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjΈ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjΛ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhWhen using }(hjH  hhhNhNubh)}(h	UBAPyPloth]h	UBAPyPlot}(hjβ  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhK!hh-hjH  hhubh*, the user now has to pass insteat of the }(hjH  hhhNhNubjA  )}(h;UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style()h]h;UBAPyPlot.style_sheet_manager.StyleSheetManager.set_style()}(hjτ  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hK!hh-hjH  hhubhW method. This passed method has to obtain one of the names of the given UBA Style Sheet}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhobjects.}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj%  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhIBelow, one can see how to set the UBA Style Sheet of vertical bar charts:}(hjH  hhhNhNubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hj@  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjS  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubh
}(hjH  hhhh-hK ubhs)}(h<br>h]h<br>}(hjf  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hK!hjH  hhubeh }(h"]h$]h&]h(]h*]uh,h
hK!hh-hhhhubj7  )}(hΆ### we set here the plot style to the UBA own costumized style sheet for creating vertical bar charts
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('v_bar_chart'))
h]hΆ### we set here the plot style to the UBA own costumized style sheet for creating vertical bar charts
plt.style.use(style_sheet_manager.StyleSheetManager().set_style('v_bar_chart'))
}hj{  sbah }(h"]h$]h&]h(]h*]languagepythonhhuh,j6  hh-hKPhhhhubh)}(hXΨ  *Code snipped that is used to set the Matplotlib Style Sheet to the individual UBA Style Sheet for plotting vertical bar charts. Insteat of passing the path to the Style Sheet or an Name, the user has to pass the ```style_sheet_manager.StyleSheetManager().set_style()```-method with the individual Style Sheet Name regarding the kind of plot.*
<br>
<br>
<br>
The basic principle is, that the ```style_sheet_manager.StyleSheetManager().set_style('v_bar_chart')```method automaticly pass the path of the recently project directory and the name of the Style Sheet to PyPlot's 
```plt.style.use()``` method. When the user run the script first time the Style Sheet will be written as ```.mplstyle``` file into the project directory where the python script is located. 
<br>
For each further call of this script this Style Sheet than will be loaded automaticly.
<br>
<br>
That means if the user wants to modify the plot style he can simply use the Style Sheet (```.mplstyle```) file and change the RC Parameter of it.
<br>
<br>
The presetted Style Sheets are defined as a *python dictionary* object in the module object ```UBAPyPlot.uba_style_sheets.styles.styles``` (see here: <a href="https://gitlab.opencode.de/OC000015999822/ubapyplot/-/blob/master/src/UBAPyPlot/uba_style_sheets/styles.py">UBAPyPlot.uba_style_sheets.styles.styles</a>)
<br>
<br>
<br>
The user can use the ```UBAPyPlot.StyleSheetManager.clean_up()``` method to automaticly remove the Mpl Style Sheet file after script has been run.h](h)}(hXO  Code snipped that is used to set the Matplotlib Style Sheet to the individual UBA Style Sheet for plotting vertical bar charts. Insteat of passing the path to the Style Sheet or an Name, the user has to pass the style_sheet_manager.StyleSheetManager().set_style()-method with the individual Style Sheet Name regarding the kind of plot.h](hΤCode snipped that is used to set the Matplotlib Style Sheet to the individual UBA Style Sheet for plotting vertical bar charts. Insteat of passing the path to the Style Sheet or an Name, the user has to pass the }(hj  hhhNhNubjA  )}(h3style_sheet_manager.StyleSheetManager().set_style()h]h3style_sheet_manager.StyleSheetManager().set_style()}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKThh-hj  hhubhH-method with the individual Style Sheet Name regarding the kind of plot.}(hj  hhhNhNubeh }(h"]h$]h&]h(]h*]uh,hhKThh-hj  hhubh
}(hj  hhhNhNubhs)}(h<br>h]h<br>}(hj³  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hjΖ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hjΩ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubh!The basic principle is, that the }(hj  hhhNhNubjA  )}(h@style_sheet_manager.StyleSheetManager().set_style('v_bar_chart')h]h@style_sheet_manager.StyleSheetManager().set_style('v_bar_chart')}(hjπ  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKThh-hj  hhubhpmethod automaticly pass the path of the recently project directory and the name of the Style Sheet to PyPlotβs}(hj  hhhNhNubh
}(hj  hhhh-hK ubjA  )}(hplt.style.use()h]hplt.style.use()}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKThh-hj  hhubhT method. When the user run the script first time the Style Sheet will be written as }(hj  hhhNhNubjA  )}(h	.mplstyleh]h	.mplstyle}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKThh-hj  hhubhD file into the project directory where the python script is located.}(hj  hhhNhNubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hj.  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhVFor each further call of this script this Style Sheet than will be loaded automaticly.}(hj  hhhNhNubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hjI  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hj\  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhYThat means if the user wants to modify the plot style he can simply use the Style Sheet (}(hj  hhhNhNubjA  )}(h	.mplstyleh]h	.mplstyle}(hjs  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKThh-hj  hhubh)) file and change the RC Parameter of it.}(hj  hhhNhNubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubh,The presetted Style Sheets are defined as a }(hj  hhhNhNubh)}(hpython dictionaryh]hpython dictionary}(hj³  hhhNhNubah }(h"]h$]h&]h(]h*]uh,hhKThh-hj  hhubh object in the module object }(hj  hhhNhNubjA  )}(h(UBAPyPlot.uba_style_sheets.styles.stylesh]h(UBAPyPlot.uba_style_sheets.styles.styles}(hjΕ  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKThh-hj  hhubh (see here: }(hj  hhhNhNubhs)}(hu<a href="https://gitlab.opencode.de/OC000015999822/ubapyplot/-/blob/master/src/UBAPyPlot/uba_style_sheets/styles.py">h]hu<a href="https://gitlab.opencode.de/OC000015999822/ubapyplot/-/blob/master/src/UBAPyPlot/uba_style_sheets/styles.py">}(hjΧ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh(UBAPyPlot.uba_style_sheets.styles.styles}(hj  hhhNhNubhs)}(h</a>h]h</a>}(hjκ  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh)}(hj  hhhNhNubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hj  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhs)}(h<br>h]h<br>}(hj'  hhhNhNubah }(h"]h$]h&]h(]h*]formathhhuh,hrhh-hKThj  hhubh
}(hj  hhhh-hK ubhThe user can use the }(hj  hhhNhNubjA  )}(h&UBAPyPlot.StyleSheetManager.clean_up()h]h&UBAPyPlot.StyleSheetManager.clean_up()}(hj>  hhhNhNubah }(h"]h$]h&]h(]h*]uh,j@  hKThh-hj  hhubhQ method to automaticly remove the Mpl Style Sheet file after script has been run.}(hj  hhhNhNubeh }(h"]h$]h&]h(]h*]uh,h
hKThh-hhhhubeh }(h"]h$]h&]h(]h*]sourceh-uh,hcurrent_sourceNcurrent_lineNsettingsdocutils.frontendValues)}(titleN	generatorN	datestampNsource_linkN
source_urlNtoc_backlinksentryfootnote_backlinksKsectnum_xformKstrip_commentsNstrip_elements_with_classesNstrip_classesNreport_levelK
halt_levelKexit_status_levelKdebugNwarning_streamN	tracebackinput_encoding	utf-8-siginput_encoding_error_handlerstrictoutput_encodingutf-8output_encoding_error_handlerjz  error_encodingutf-8error_encoding_error_handlerbackslashreplacelanguage_codeenrecord_dependenciesNconfigN	id_prefixhauto_id_prefixiddump_settingsNdump_internalsNdump_transformsNdump_pseudo_xmlNexpose_internalsNstrict_visitorN_disable_configN_sourceh-_destinationN_config_files]file_insertion_enabledraw_enabledKline_length_limitM'pep_referencesNpep_base_urlhttps://peps.python.org/pep_file_url_templatepep-%04drfc_referencesNrfc_base_url&https://datatracker.ietf.org/doc/html/	tab_widthKtrim_footnote_reference_spacesyntax_highlightlongsmart_quotessmartquotes_locales]character_level_inline_markupdoctitle_xformdocinfo_xformKsectsubtitle_xformimage_loadinglinkembed_stylesheetcloak_email_addressessection_self_linkenvNubreporterNindirect_targets]substitution_defs}(wordcount-wordsh	substitution_definition)}(h619h]h619}hjΈ  sbah }(h"]h$]h&]wordcount-wordsah(]h*]uh,jΆ  hh-ubwordcount-minutesj·  )}(h3h]h3}hjΘ  sbah }(h"]h$]h&]wordcount-minutesah(]h*]uh,jΆ  hh-ubusubstitution_names}(wordcount-wordsj΅  wordcount-minutesjΗ  urefnames}refids}nameids}	nametypes}h"}footnote_refs}citation_refs}autofootnotes]autofootnote_refs]symbol_footnotes]symbol_footnote_refs]	footnotes]	citations]autofootnote_startKsymbol_footnote_startK 
id_countercollectionsCounter}Rparse_messages]transform_messages]transformerNinclude_log]
decorationNhh
myst_slugs}ub.