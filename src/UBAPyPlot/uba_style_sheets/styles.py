styles = {

    'standard_chart' :
    
'''


#### MATPLOTLIB RC FORMAT

#================================================================#
# Titel  : UBAnized MPL-style-sheet for plotting standard charts #
# Author : Alexander Prinz                                       #
# Email  : alexander.prinz@uba.de                                #
# Date   : 04.08.2023                                            #
#================================================================#



## ***************************************************************
## * AXES
## ***************************************************************

### UBAnized

## thickness of the axes spines
axes.linewidth:      .5
axes.spines.top   : False
axes.spines.right : False 
axes.spines.left  : False
  
axes.titlelocation:  left
axes.titley:         1.0
axes.titlepad:       -25
axes.labelsize:      7
axes.labelpad:       4
axes.axisbelow      : True



## ****************************************************************
## * TICKS                                                          
## ****************************************************************

xtick.labelsize:     6     
ytick.labelsize:     6     
xtick.minor.size:    3     
xtick.minor.width:   .8 
xtick.major.size:    3     
ytick.minor.size:    0     
ytick.major.size:    0 



## ****************************************************************
## * GRIDS
## ****************************************************************

grid.color:          darkgrey  




## ****************************************************************
## * LEGEND
## ****************************************************************

legend.facecolor:    (0.0, 0.0, 0.0, 0.0)  
legend.fontsize:     8



## ****************************************************************
## * FIGURE
## ****************************************************************

figure.dpi:          150       
figure.facecolor:    (0.0, 0.0, 0.0, 0.0)     



## ****************************************************************
## * SAVING FIGURES
## ****************************************************************

savefig.transparent: True   
'''

    ,


    'line_chart' :
    
'''


#### MATPLOTLIB RC FORMAT

#=================================================================#
# Titel  : UBAnized MPL-style-sheet for plotting line charts      #
# Author : Alexander Prinz                                        #
# Email  : alexander.prinz@uba.de                                 #
# Date   : 03.10.2022                                             #
#=================================================================#



## ****************************************************************
## * AXES
## ****************************************************************

### UBAnized

## thickness of the axes spines
axes.linewidth:      .5
axes.spines.top   : False    
axes.spines.right : False 
axes.spines.left  : False
  
axes.titlelocation:  left   			 
axes.titley:         1.0    
axes.titlepad:       -25    
axes.labelsize:      7      
axes.labelpad:       4   
axes.axisbelow      : True



## ****************************************************************
## * TICKS
## ****************************************************************

xtick.labelsize:     6     
ytick.labelsize:     6     
xtick.minor.size:    3     
xtick.minor.width:   .8 
xtick.major.size:    3     
ytick.minor.size:    0     
ytick.major.size:    0 



## ****************************************************************
## * GRIDS
## ****************************************************************

grid.color:          darkgrey  




## *****************************************************************
## * LEGEND
## *****************************************************************

legend.facecolor:    (0.0, 0.0, 0.0, 0.0)  
legend.fontsize:     8



## *****************************************************************
## * FIGURE
## *****************************************************************

figure.dpi:          150       
figure.facecolor:    (0.0, 0.0, 0.0, 0.0)     



## *****************************************************************
## * SAVING FIGURES
## *****************************************************************

savefig.transparent: True   
'''

,

    'v_bar_chart' : '''
    


#### MATPLOTLIB RC FORMAT

#===================================================================#
# Titel  : MPL-style-sheet for plotting vertical Bar Charts         #
# Author : Alexander Prinz                                          #
# Email  : alexander.prinz@uba.de                                   #
# Date   : 13.12.2022                                               #
#===================================================================#



## ******************************************************************
## * AXES
## ******************************************************************

### UBAnized

## thickness of the axes spines
axes.linewidth     : .6
axes.spines.top    : False    
axes.spines.right  : False 
axes.spines.left   : True
  
axes.titlelocation : left   			 
axes.titley        : 1.0    
axes.titlepad      : -25    
axes.labelsize     : 6      
axes.labelpad      : 4   
axes.axisbelow     : True



## *******************************************************************
## * TICKS
## *******************************************************************

xtick.labelsize   : 6     
ytick.labelsize   : 6     
xtick.minor.size  : 3     
xtick.minor.width : .6 
xtick.major.width : .6
ytick.major.width : .6
xtick.major.size  : 2     
ytick.minor.size  : 0     
ytick.major.size  : 2 



## *******************************************************************
## * GRIDS
## *******************************************************************

grid.color        : darkgrey  





## *******************************************************************
## * LEGEND
## *******************************************************************

legend.facecolor  : (0.0, 0.0, 0.0, 0.0)  
legend.fontsize   : 8



## *******************************************************************
## * FIGURE
## *******************************************************************

figure.dpi        : 150       
figure.facecolor  : (0.0, 0.0, 0.0, 0.0)     



## *******************************************************************
## * SAVING FIGURES
## *******************************************************************

savefig.transparent: True      
    
'''

,

    'h_bar_chart' : '''
    




#### MATPLOTLIB RC FORMAT

#=====================================================================#
# Titel  : MPL-style-sheet for plotting in UBA Corporate Design       #
# Author : Alexander Prinz                                            #
# Email  : alexander.prinz@uba.de                                     #
# Date   : 13.12.2022                                                 #
#=====================================================================#



## ********************************************************************
## * AXES
## ********************************************************************

### UBAnized

## thickness of the axes spines
axes.linewidth     : .6
axes.spines.top    : False    
axes.spines.right  : False 
axes.spines.left   : False
axes.spines.bottom : False
  
axes.titlelocation : left   			 
axes.titley        : 1.0    
axes.titlepad      : -25    
axes.labelsize     : 6      
axes.labelpad      : 1  
axes.axisbelow     : True



## *********************************************************************
## * TICKS
## *********************************************************************

xtick.labelsize   : 6     
ytick.labelsize   : 6     
ytick.labelpad    : 4 
   
xtick.major.size  : 1     
xtick.major.width : .5     
ytick.major.size  : 8 
ytick.major.width : .6


## *********************************************************************
## * GRIDS
## *********************************************************************

grid.color        : darkgrey  





## *********************************************************************
## * LEGEND
## *********************************************************************

legend.facecolor  : (0.0, 0.0, 0.0, 0.0)  
legend.fontsize   : 8



## *********************************************************************
## * FIGURE
## *********************************************************************

figure.dpi        : 200       
figure.facecolor  : (0.0, 0.0, 0.0, 0.0)     



## *********************************************************************
## * SAVING FIGURES
## *********************************************************************

savefig.transparent: True      
   
    
'''


}


if __name__ == '__main__':
    print(styles)