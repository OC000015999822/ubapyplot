import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from math import ceil
from pathlib import Path
import matplotlib as mpl
import os
import warnings


# we load the plugin <grid> from the submodule directory <mod>
from .plugins import grid





class UBA_Plot:
    '''
    Main class of the module.
    It maps an API to the well-known plotting framework 
    `Matplotlib's Pyplot <https://matplotlib.org/3.5.1/api/_as_gen/matplotlib.pyplot.html>`_.
    
    The basic concept is to create two coherent axes objects. 
    The first one (called `Parent Axes Object`) contains all text elements 
    to describe the plot figure like title, subtitle, figure number, sources and footnote.
    The setter-methods of the class are used to define the text elements of the `Parent Axes Object`.
    
    In addition, this objeject also contains the diagonal grey hatched UBA stripes, that is mapped by a certain class method.

    The second object is the `Child Axes Object`. 
    This object catains all graphical elements and represents the diagramm or graph or what the user wants to plot.
    This is treated in the class as `axes.inset()` object to the corresponding `Axes Parent Object`. 
    Both objects are instantiated via the constructor and the axes child object 
    can be handled as an attribute of the class outside of the class in a main script 
    as one is used to from `Matplotlib Pyplot`.
    '''

    def __init__(
        self, 
        size=None, 
        height_child=None, 
        width_child=None, 
        bbox_to_anchor=None, 
        foreign_child_ax=None, 
        projection=None,
        y_line_1=.08,
        y_line_2=.16,
        y_line_3=.8,
        y_line_4=.9158,
        subplot_grid=None
        ):
    
        
    
        r'''
        Constructor obtains some but only optional attributes from outside the class object instance.
        
        
        
        Parameters
        
        ----------
        
        
            size : :obj:`tuple` (float, float) 
                Is used to define the size of the figure (Parent Axes Object) by its values of width and height.
                
                `Value range`: :math:`ℝ^{+}\timesℝ^{+}` | `default`: (7. 5, 4.75)
                
            |
            |                
                
            height_child: :obj:`str`
                Is used to set the relative height of the Child Axes Object regarding its Parent Axes Object.
                The value expression is defined as string of a percent number by a following percent character (example: ``height_child="42%"``). 
                The respective value range of the number component of this string expression is -of course- :math:`(0, 100]`.                

            |
            |

            width_child: :obj:`str`
                Is used to set the relative width of the Child Axes Object regarding its Parent Axes Object.
                The value expression is defined as string of a percent number by a following percent character (example: ``width_child="69%"``). 
                The respective value range of the number component of this string expression is -of course- :math:`(0, 100]`. 

                .. note::  
                    **The relative width means the relation to the width of the whole parent axes object and not only the grey hatched area. 
                    That is why diagrams are way too large when using a 100% setting.**
        
            |
            |       

            bbox_to_anchor: :obj:`tuple`
                Is used to set the boundary box of the Child Axes Object.
                
                `Value range`: :math:`(a, b, c, d);~\forall~a, b, c, d~|~a, b, c, d \in [-1, 1]` | `default`: ``(0, 0, 1, 1)``
                
            |
            |            
            
            foreign_child_ax : :obj:`Matplotlib.PyPlot.Axes-Object`
                An `Matplotlib`-Axes-Object that has been passed from outside.
                
            |
            |                
                
            projection : :obj:`str`    
                Value to set the projection scheme of the coordinate system of diagram.
                
                `Value range`: :math:`\in` {None, "polar"} | `default`: None :math:`\rightarrow` cartesian coordinate system
                
            |
            |                
                
            y_line_1 : :obj:`float`
                Value to set the height of the first horizontal line element.
                
                `Value range`: :math:`\in` [0, 1] | `default`: ``.8``

            |
            |

            y_line_2 : :obj:`float`
                Value to set the height of the second horizontal line element.
                
                `Value range`: :math:`\in` [0, 1] | `default`: ``.16``        
        
            |
            |
            
            y_line_3 : :obj:`float`
                Value to set the height of the third horizontal line element.
                
                `Value range`: :math:`\in` [0, 1] | `default`: ``.8``
                
            |
            |                
                
            y_line_4 : :obj:`float`
                Value to set the forth of the third horizontal line element.
                
                `Value range`: :math:`\in` [0, 1] | `default`: ``.9158``                
 
        
        |
        |
        
        '''
        
        
        ### the grid on which optional matrix plots has to be placed
        self.subplot_grid = subplot_grid
        
        
        ### we warn user if he has defined the height_child and/or width_child attributes although he has defined a grid plot       
        warn_txt = '''You have defined the child size attributes although you also have defined a grid plot.
        Your child size attributes will be ignored!...
        '''
        if subplot_grid and (height_child or width_child):
            warnings.warn('\n\n' + warn_txt + '\n\n')
            
        
        ### we warn user if he has defined the projection attribute although he has defined a grid plot       
        warn_txt = '''You have defined the projection attribute although you also have defined a grid plot 
        with specific projection definitions for each child axes object.
        Your your projection will be ignored!...
        '''
        if self.subplot_grid:
            if "projection" in self.subplot_grid.keys() and projection:
                warnings.warn('\n\n' + warn_txt + '\n\n')
        

        
        

        ### instantiate an axes object (is now called parent axes object)
        self.size = size   # obtional attribute to alter the aspect ratio of plot figure 
        if self.size == None:
            self.size = (15/2, 9.5/2)  # the size attributes where taken from the UBA Corporate Design guideline
            self.fig, self.parent_ax = plt.subplots(figsize=self.size) # set default size of plot figure
        else:
            self.fig, self.parent_ax = plt.subplots(figsize=(self.size[0], self.size[1])) # otherwise use user setup
 
        self.bbox_to_anchor = bbox_to_anchor
        if self.bbox_to_anchor == None:
            self.bbox_to_anchor = (0.015, 0, 1, 1)
        else:
            self.bbox_to_anchor = bbox_to_anchor
 
 
        ### we set a norm vector (determined by the information about the UBA CD style guideline)
        self.norm = (15/2, 9.5/2)
 
        ### set off all axis elements of parent axes object
        self.parent_ax.set_axis_off()
        
        ### set the backgroundcolor to None -> fully transparency
        self.parent_ax.set_facecolor('None')      
              
        
              
        ### we set here the relative y-position of the 4 style lines first on is the lowest line
        self.y_line_1 = y_line_1
        self.y_line_2 = y_line_2
        self.y_line_3 = y_line_3
        self.y_line_4 = y_line_4
        
        
        
        ### draw the lines
        self.__draw_4_lines()


        ### width and hight of the child axes object
        if height_child == None:
            self.height_child = '45%'
        else:
            self.height_child = height_child

        if width_child == None:
            self.width_child = '90%'
        else:
            self.width_child = width_child



        ### make the child axes object if no foreign child axes object has been passed
       
        if foreign_child_ax == None:
            if projection != None:
                self.child_ax = self.__mk_child_axes_object(projection=projection)
            else:
                self.child_ax = self.__mk_child_axes_object()
        else:
            self.child_ax = foreign_child_ax
            


     
    def __mk_child_axes_object(self, projection=None):
        '''
        Private method to generate a child axes object.
        Method is called automaticly by the constructor 
        when the user does not pass a foreign axes object.
        
        The workflow is based on to matplotlib.axes.Axes.inset_axes() method.
        `See here for more details: <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.inset_axes.html>`_.
        
        Parameters
        
        ----------
        
            projection : :obj:`str`
                The parameter defines whether the projection scheeme is euclidic (no parameter passed) or 
                polar. In case of polar coordinates the parameter value has to be <polar>.
                The parameter is passed through to the inset axes object.
        
        
        Returns
        
        -------
        
            axes : :obj:`matplotlib.pyplot.axes object`
                The child axes object that represents the actual plot window. It is
                just an inset axes object embeded into the parent axes object.
                
        |
        |
        
        '''

        if self.subplot_grid == None:  # CASE: user has not subplot option defined (one child axes object within parent)

            ### we instantiate an inset axes object that is placed into the parent 
            ### axes object
            if projection == None:
                self.axes = inset_axes(
                    self.parent_ax, 
                    width=self.width_child, 
                    height=self.height_child, 
                    loc='center',
                    bbox_to_anchor=self.bbox_to_anchor,
                    bbox_transform=self.parent_ax.transAxes,
                    )   
                
            if projection == 'polar':
                self.axes = inset_axes(
                    self.parent_ax, 
                    width=self.width_child, 
                    height=self.height_child, 
                    loc='center',
                    bbox_to_anchor=self.bbox_to_anchor,
                    bbox_transform=self.parent_ax.transAxes,
                    axes_class = mpl.projections.get_projection_class('polar'),
                    )   

            ### we set the facecolor to 'None' to get a fully transparent background and make the
            ### UBA stripes from laying down paranet exes object visible 
            self.axes.set_facecolor('None')

            return self.axes
            
            
        else: # CASE: user has  subplot option defined (multiple child axes objects within parent)
            return grid.Grid(self.subplot_grid, self.parent_ax).get_grid()
                            
               
             
               
        
    def __draw_4_lines(self):
        '''
        Private method to draw the 4 horizontal styling lines.
        
        |
        |
        
        '''

        
        ### draw 1'th line 
        self.parent_ax.axhline(self.y_line_1, lw=1, c='k')
        
        ### draw 2'nd line
        self.parent_ax.axhline(self.y_line_2, lw=.5, c='k')
        
        ### draw 3'rd line
        self.parent_ax.axhline(self.y_line_3, lw=.5, c='k')        

        ### draw 4'th line
        self.parent_ax.axhline(self.y_line_4, lw=1, c='k') 
 
 
    def __compute_y_shift(self, text):
        '''
        Private method to compute automaticly the positive y-direction shift 
        in terms of the footnote and source info text to get the correct distance
        between the letters of the first line to the first style line.
        The method recognizes the number of given lines of the text.
        '''
        
        n_lines = text.count('\n') + 1  # number of text lines
        a = self.y_line_1 # distance from bottom to first style line
        b = 0.01          # relative distance between letters of first text line and first style line       
        
        return abs(n_lines-3)*((a-b)/3)
 
 
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~# 
 
 
 
    def get_child_ax(self):
        '''
        Method to obtain the child axes object. 
        The method has to be called and returns the axes object.
        This axes object than can be threated like the normal axes object that
        is returned when calling 
        
        
        ``fig, ax = matplotlib.pyplot.subplots()``. 

        See more about axes objects here:
        
        `LINK <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.subplots.html>`_
       
        
        To use the way of this workflow to obtain the axes object please use instead of the 
        subplots() method following code:
        
        .. code-block:: python
        
            # import the UBA_CD_PyPlot-Package
            from UBA_CD_PyPlot import UBA_Plot
            
            # instantiate an UBA_CD_PyPlot object
            plot_obj = UBA_Plot.UBA_Plot()
            
            # get the axes object from the plot object
            ax = plot_obj.get_child_ax()
            
            
            
        
        Returns
        
        -------
        
            self.child_ax : :obj:`matplotlib.axes`
                The automaticly instantiated axes object. This object
                can be used outside within the main script to make the
                plots in it.
                
        |
        |
            
        '''
        
        return self.child_ax
 
 
 
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~# 
  
        
  

    def set_fig_number_text(self, text=None, xy=(0, .93), fontsize=7):   
        '''
        Sets the figure number text.
            
        Parameters    
        
        ----------
            
                text : :obj:`str`
                    The text definition that should be printed.
                    
                xy : :obj:`tuple` | `default` : ``(0, .93)``
                    Tuple of 2 float to set the coordinate where the text should be printed.
                    
                fontsize : :obj:`float` | `default` : ``7``
                    Number to set the fontsize.
                    
        |
        |
        
        '''
        self.parent_ax.text(
            xy[0], 
            xy[1], 
            text, 
            fontfamily='calibri',            
            fontsize=fontsize,
            zorder=0
            )

                
        
    def set_title_text(self, text=None, xy=(0, .87), fontsize=11): 
        '''
        Sets the figure title text.
            
        Parameters
        
        ----------
            
                text : :obj:`str`
                    The text definition that should be printed.
                    
                xy : :obj:`tuple` | `default` : ``(0, .861)``
                    Tuple of 2 float to set the coordinate where the text should be printed.
                    
                fontsize : :obj:`float` | `default` : ``11``
                    Number to set the fontsize.
                    
        |
        |
        
        '''   
        self.parent_ax.text(
            xy[0], 
            xy[1], 
            text, 
            fontfamily='calibri',
            fontweight='bold',
            fontsize=fontsize,
            zorder=0
            )



    def set_subtitle_text(self, text=None, xy=(0, .82), fontsize=9): 
        '''
        Sets the figure subtitle text.
            
        Parameters
        
        ----------
            
                text : :obj:`str`
                    The text definition that should be printed.
                    
                xy : :obj:`tuple` | `default` : ``(0, .82)``
                    Tuple of 2 float to set the coordinate where the text should be printed.
                    
                fontsize : :obj:`float` | `default` : ``9``
                    Number to set the fontsize.
                    
        |
        |
        
        '''    
        self.parent_ax.text(
            xy[0], 
            xy[1], 
            text, 
            fontfamily='calibri', 
            fontsize=fontsize,
            zorder=0
            )



    def set_footnote_text(self, text=None, y_shift=None, fontsize=6):
        '''
        Sets the figure footnote text. 
            
        Parameters
        
        ----------
            
                text : :obj:`str`
                    The text definition that should be printed.
                    
                y_shift : :obj:`float` | `default` : ``None``
                    Number to shift the text upwards along the y-axis.
                    It is by default setted to None and will be atomatically fitted
                    when no values has been passed.
                    
                fontsize : :obj:`float` | `default` : ``6``
                    Number to set the fontsize.
                    
        |
        |
        
        '''  
        if y_shift:    #CASE: The y shift upwards is defined by the user
            self.parent_ax.text(
                0, 
                y_shift, 
                text, 
                fontfamily='calibri', 
                fontsize=fontsize,
                zorder=0,
                horizontalalignment='left',        #NEW in 1.3 automatic left sided text alignment 
                transform=self.parent_ax.transAxes #NEW in 1.3 automatic left sided text alignment 
                )

        else:          #CASE: The y shift is automaticly computed when use did not pass a y shift value
            self.parent_ax.text(
                0, 
                self.__compute_y_shift(text), 
                text, 
                fontfamily='calibri', 
                fontsize=fontsize,
                zorder=0,
                horizontalalignment='left',        #NEW in 1.3 automatic left sided text alignment 
                transform=self.parent_ax.transAxes #NEW in 1.3 automatic left sided text alignment 
                )





    def set_source_info_text(self, text=None, y_shift=None, fontsize=6):
        '''
        Sets the figure source info text.
        There is no possibility to get an automatic text align to the right side from the plot. 
        Since, it has to be setted by own by using the `xy` parameter. Use the first value (x-direction)
        of the parameter tuple to shift the text along the x direction. The larger the value till 1 the
        more is the text shifted to the right.
        Use values between 0 and 1.
            
        Parameters
        
        ----------
            
                text    : :obj:`str`
                    The text definition that should be printed.
                    
                y_shift : :obj:`float` | `default` : ``None``
                    Number to shift the text upwards along the y-axis.
                    It is by default setted to None and will be atomatically fitted
                    when no values has been passed.
                    
                fontsize : :obj:`float` | `default` : ``6``
                    Number to set the fontsize.
                    
                linespacing : :obj: `float` | `default` : ``1.1``
                    Number to set the spacing between broken text lines.
                    
        |
        |
        
        '''  
        
        if y_shift:              # CASE: y_shift parameter is given by user from outside
            self.parent_ax.text(
                1, 
                y_shift, 
                text, 
                fontfamily='calibri', 
                fontsize=fontsize,
                zorder=0,
                horizontalalignment='right',        #NEW in 1.3 automatic right sided text alignment 
                transform=self.parent_ax.transAxes, #NEW in 1.3 automatic right sided text alignment 
                ) 
        else:                    # CASE: y_shift has to be computed by the method <__compute_y_shift>       
            self.parent_ax.text(
                1, 
                self.__compute_y_shift(text), 
                text, 
                fontfamily='calibri', 
                fontsize=fontsize,
                zorder=0,
                horizontalalignment='right',        #NEW in 1.3 automatic right sided text alignment 
                transform=self.parent_ax.transAxes, #NEW in 1.3 automatic right sided text alignment 
                )             
 


    def set_stripes(self, df=.006, color='#cdd7da', lw=.5):
        '''
        Method to draw the UBA style hatch stripes into the parent axes object.
        The method draws automaticly the hatch stripes in correct manner. You do not 
        need to set any parameters but you can nethertheless alter the line distance
        by the parameter `df` (distance factor), The line color is by default setted 
        to the UBA stripes grey (hexadecimal definition = #cdd7da). with the `lw` 
        parameter the line width can be setted but the default setting of 0.5 units 
        should be the right.

        Call simply the `set_stripes()` method onto the UBA_Plot object without any arguments
        to obtain the default settings or use your own parametrization.
        
        If you do not call this method, your plot window will remain white, or actually transparent.
        
        
        
        Parameters
        
        ----------
        
            df : :obj:`float` | `default` : ``0.006``
                Factor to define the line distance between each stripe.
                
            color : :obj:`str` | `default` : ``#cdd7da``
                Defines the line color of each UBA stripe.
                
            lw : :obj:`float` | `default` : ``0.5``
                Defines the linewidth of each UBA stripe.
                
        |
        |
        
        '''

        
 
        ### get an distance between xi and xi+1 so that the slope of the lines is 
        ### in every case 1 -> we have to transform be the following factor
        Dx = (self.y_line_3 - self.y_line_2)*(self.size[1]/self.size[0])

        ### we compute a correcture factor that uses the norm x-axis definition to transform
        ### the stripe distance in respect to the the aspect ratio (figure size)
        cor_fac = self.norm[0]/self.size[0]

        ### we get the minimum number of stripe iterations in positive direction
        b = ceil(1/(df*cor_fac))

        ### we get the minimum number of stripe iterations in negative direction
        a = -1*b*ceil(self.size[1]/self.size[0])

        ### we draw each of the stripes 
        for n in range(a, b):
            
            self.parent_ax.plot(
                [df*cor_fac*n , (df*cor_fac*n + Dx)],
                [self.y_line_2, self.y_line_3*0.997],  # correcture factore to avoid the line to across the 3'rd style line
                color=color, 
                lw=lw,
                zorder=1
                )

        ### we set the relative parent axes size to 0 til 1 for each dimension to avoid
        ### overfalling of the base size induced by the drawn stripes.
        self.parent_ax.set_xlim(0, 1)
        self.parent_ax.set_ylim(0, 1)
            
                



    

    def mk_legend(self, ncol=999, loc=None, fontsize=7, shift_fac=1.2, custom_handles=None):
        '''
        Function to activate the legend.
        Two options can be used.
        
        a) The legend elements can be inherited from the child axes object.
        
        b) The legend elements can be passed as custom handles (optional). 
        
        That can be necessary when the handling of the legend elements from the child axes object 
        is not possible or problematicly. The user than can be costumize own handles.
        
        Fore more details see the link below:
        
        `LINK <https://matplotlib.org/stable/gallery/text_labels_and_annotations/custom_legends.html>`_
        
        |
        |
        
        Parameters
        
        ----------
        
            ax_ : :obj:`matplotlib.pyplot-Axis-Object`
                The child axis object that has included the actually plot.
                
            ax : :obj:`matplotlib.pyplot-Axis-Object`
                The parent axis object what we instatiate by plt.subplots().   
            
            ncol : :obj:`int` | `default` : ``999``
                Paramter to set the number of columns of the legend frame.
                If the number is smaler than the number of plot objects
                than we obtain more lines. 
                
            loc : :obj:`str`
                Parameter to set the location of the legend. The
                default setting is a lower left located position and
                corresponds to the UBA style.
                
            fontsize : :obj:`float` | `default` : ``7``
                Defines the fontsize of the legend text label.
                
            shift_fac : :obj:`float` | `default` : ``1``
                Parameter can be used to shift the legend elements along the vertical direction.
                Use values smaller than 1 to shift downwards and values larger than 1 to shift upwards.
                
            costum_handles : obj:`list`
                List of corresponding matplotlib patch objects and their text labels to define the 
                costum handle.
                Users can find informations in the documentation but here too:
                
                
                
                
        |
        |
        
        '''
    
        shift_fac = 1/shift_fac # we do here a transformation of the passed shift factor so that passed values larger than 1 menas an upwards shift
    
        ### we define the default legend location that should fulfill most cases
        if loc == None:
            loc = (0, (self.y_line_2 - self.y_line_1)/shift_fac)
    
        ### get legend handles/labels from child axis object and pass it to the parent axes object
        if custom_handles != None:
            self.parent_ax.legend(handles=custom_handles, ncol=ncol, frameon=False, loc=loc, prop={'size': fontsize})
            
        ### if user passes costumized legend that legend will be passed insteat
        else:
            if type(self.child_ax) == list: # CASE: We have a subplots with some childs and grab their handles and labels 
                H = []
                L = []
                
                for ax_obj in self.child_ax:
                    h, l = ax_obj.get_legend_handles_labels()
                    print(h)
                    print(H)
                    print(l)
                    print(L)
                    
                    H = H + h
                    L = L + l
                
            else:
                H, L = self.child_ax.get_legend_handles_labels()   
                
            self.parent_ax.legend(H, L, ncol=ncol, frameon=False, loc=loc, prop={'size': fontsize})






