import numpy as np
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib as mpl

class Grid:
    '''
    The class mapps a functionality to plot more than one Axes Child Object within the Parent Axes Object.
    That is basicly known as matrix plots or subplots. Unfortunately the Matplotlib API does not 
    provide a functionality to use the well known subplots function. 
    This class provides a workaround so that users be able to make subplots nevertheless.

    |
    |
    
    '''
    

    def __init__(self, grid_props, parent_ax):
        self.grid_props = grid_props
        self.parent_ax  = parent_ax
        
    
        ### make the grid array that contains the inset axes object 
        self.__make_inset_axes_objects()


       
    ### ~~~~~~~~~~~~~~~~~~~~~~ private methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __make_inset_axes_objects(self):
        '''
        Private method to generate all Child Axes objects.
        Those Child Axes Object are stored within a list that is created as class attribute `self.__grid_arr`.
        
        Method is called automaticly by the constructor. 
                
        |
        |
        
        '''    
    
    
    
    
        self.__grid_arr = [np.nan for _ in range(len(self.grid_props["width"]))]
        
        if 'projection' in self.grid_props.keys(): # CASE: user wants to use explicit statements of using specific projection
            
            for i in range(len(self.grid_props["width"])):
            
                width  = self.grid_props["width"][i]
                height = self.grid_props["height"][i]
                bbox   = self.grid_props["bbox"][i]
                proj   = self.grid_props["projection"][i]
                
                ## we make the shift correction if user does it want
                if "global_shift" in self.grid_props.keys():
                
                    # x-direction
                    if self.grid_props["global_shift"][0] != 0:             
                        bbox[0] = bbox[0] + self.grid_props["global_shift"][0]
                        
                    # y-direction    
                    if self.grid_props["global_shift"][0] != 0:
                        bbox[1] = bbox[1] + self.grid_props["global_shift"][1]
                        
                        
                ### we make each i'th inset axes object 
                self.__grid_arr[i] = inset_axes(
                    self.parent_ax, 
                    width=width, 
                    height=height, 
                    loc=3,
                    bbox_to_anchor=bbox,
                    bbox_transform=self.parent_ax.transAxes,  
                    axes_class = mpl.projections.get_projection_class(proj)
                )
                
                ### we set here the background color to None to obtain 100% transparent child object
                ax_obj.set_facecolor('None')
                
                ### we add the child axes object to the list
                self.__grid_arr[i] = ax_obj
                
                
                
        else: # CASE user has not defined the projection shemes and cartesian coordinates where uesd as default setting
        

            for i in range(len(self.grid_props["width"])):
            
                width  = self.grid_props["width"][i]
                height = self.grid_props["height"][i]
                bbox   = self.grid_props["bbox"][i]
                
                ## we make the shift correction if user does it want
                if "global_shift" in self.grid_props.keys():
                
                    # x-direction
                    if self.grid_props["global_shift"][0] != 0:             
                        bbox[0] = bbox[0] + self.grid_props["global_shift"][0]
                        
                    # y-direction    
                    if self.grid_props["global_shift"][0] != 0:
                        bbox[1] = bbox[1] + self.grid_props["global_shift"][1]
                        
                        
                ### we make each i'th inset axes object 
                ax_obj = inset_axes(
                    self.parent_ax, 
                    width=width, 
                    height=height, 
                    loc=3,
                    bbox_to_anchor=bbox,
                    bbox_transform=self.parent_ax.transAxes,                  
                )          
           
                ### we set here the background color to None to obtain 100% transparent child object
                ax_obj.set_facecolor('None')
                
                ### we add the child axes object to the list
                self.__grid_arr[i] = ax_obj    
    ### ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ getter ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
    def get_grid(self):
        '''
        Getter-Method to handle the list of Child Axes Objects.

       
        
        
        Returns
        
        -------
        
            grid_array : :obj:`list`
                List-Object that has included all Child Axes Objects as its elements.
                
        |
        |
        
        '''    
    
    
    
    
    
        return self.__grid_arr
        

    