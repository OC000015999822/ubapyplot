import os, sys
import matplotlib as mpl
from pathlib import Path
from matplotlib import pyplot as plt
import warnings

from .uba_style_sheets import styles


class StyleSheetManager:


    def __init__(self):
   
        ### we load the style sheet definitions in Form of a dictionary
        self.name_dict = self.__load_styles()



    def set_style(self, style_kind=None, force_overwr=False): 
        '''
        Will generate the corresponding stylesheet into the working directory and pass to the plt.style.use() method
        of Matplotlib.
        
        
        Parameters
        
        ----------
        
            style_kind:obj:`str`
                Name of the Style Sheet Tamplate that should be used.
                
            force_overwr:obj:`str` default:False
                Flag to define whether the generated mplstyle file should be overwritten. 
                
                
        
        Returns
        
        -------
        
            path : :obj:`str`
                path to the style sheet in workingdirectory.
                
        |
        |
        
        '''
        
        if not style_kind:
            print(80*'!' + '\n\nPlease define the parameter <style_kind>!\n\n\n')
            
            print('Following styles are available:\n\n')
            for style_name in list(self.name_dict.keys()):
                print(style_name)
                
            print('\n\n' + 80*'!')
        
        
    
        if os.path.exists(os.path.join(os.getcwd(), style_kind + '.mplstyle')) and force_overwr==True or not os.path.exists(os.path.join(os.getcwd(), style_kind + '.mplstyle')):
            with open(os.path.join(os.getcwd(), style_kind + '.mplstyle'), 'w') as fout:
                print(os.path.join(os.getcwd(), style_kind + '.mplstyle'))
                fout.write(self.name_dict[style_kind])     

            return os.path.join(os.getcwd(), style_kind + '.mplstyle')    
            
                
        elif os.path.exists(os.path.join(os.getcwd(), style_kind + '.mplstyle')) and force_overwr==False:
            return os.path.join(os.getcwd(), style_kind + '.mplstyle') 
            
            
            
            
            
    def __load_styles(self):
        '''
        Method to load the style sheet definitions.
        '''
        return styles.styles
        
        

    def clean_up(self):
        '''
        Method to remove the mplstyle sheet file.
        '''
        for fname in os.listdir(os.path.join(os.getcwd())):
            if fname.endswith('.mplstyle'):
                os.remove(os.path.join(os.getcwd(), fname))
        
        


if __name__ == '__main__':
    
    obj = StyleSheetManager()
    
    obj.set_style('line_chart')









