# UBAPyPlot Package


This *Python*-Package containes a *Matplotlib.pyplot*-wrapper to make plots/diagrams easy with 
*Matplotlib* directly in the UBA corporate design.

<br>

The Package containes following 3 members:

	* UBA_Plot.py
	* style_sheet_manager
	* grid (within the directory *plugins*)
	* the style sheet definitions (within the directory *uba_style_sheets*)

<br>


You can use the *setup.py* to make a *python*-wheel to install all stuff simply by <cite>PIP</cite> or <cite>conda</cite>.
To use the setup file enshure to have already installed *wheel* with <cite>PIP</cite>.

Run following prompt in CMD/(Power)Shell/Bash in 'src/' to brew a wheel:

```shell
python setup.py bdist_wheel
```



