#=======================================#
# Title : UBAPyPlot                     # 
# Author: Alexander Prinz               # 
# Email : alexander.prinz@uba.de        # 
# Date  : 05.07.2023                    # 
#=======================================#

import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="UBAPyPlot",
    version="1.4",
    author="Alexander Prinz",
    author_email="alexander.prinz@uba.de",
    description=("Matplotlib.pyplot-wrapper for plotting UBA Corporate Design complianted figures"),
    license="MIT",
    packages=setuptools.find_packages(),
    install_requires=[],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: Microsoft :: Windows",
    ],
    python_requires='>=3.7',
)