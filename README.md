# UBAPyPlot



<i>UBAPyPlot</i> is a “nano-framework” based on <i>Matplotlib</i> for creating plots in the <i>UBA</i> Corporate Design.
<br> 
When working with this framework, many techniques and workflows remain the same or very similar to the regular use of Matplotlib’s Pyplot.

<br>
<br>

It is aimed at internal employees as well as third parties who work on behalf of UBA and have to create illustrations/data visualizations/plot figures in the UBA corporate design
by using <i>Python</i>.
<br>
<br>
<br>

For more informations please see the documentation here:

<br>

<a href="https://oc000015999822.usercontent.opencode.de/ubapyplot/" ><p style="font-size: 25px"><u>UBAPyPlot - Documentation</u></p></a>

<br>
<br>
<br>

## Installation

Please have a look at following link to see the how to install the package:


<a href="https://oc000015999822.usercontent.opencode.de/ubapyplot/#installation-2" ><p style="font-size: 25px"><u>Installation</u></p></a>



